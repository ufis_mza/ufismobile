rem This file is to generate the EAR file which is to be deployed to GlassFish server : KO PHYOE UPDATE
rem Absolute paths are used here
rem Therefore this can cause the problem if the paths on are not the same in a development environment
rem 7z software is required to run this file
rem If 7z is not installed then a similar software for file zipping function is needed and the command regarding to calling 7z should be amended accordingly too

copy /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\index.html D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0
copy /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\app.config.js D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0
copy /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\app.css D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0
copy /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\app.js D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0

rem copying the files which are very rare to get updated
rem if not exist D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\css mkdir D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\css
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\css D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\css 
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\images D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\images
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\js D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\js
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\layouts D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\layouts

rem copying the files which are very core and always being updated as needed
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\views D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\views
xcopy /i/k/e/y D:\sandbox\UfisMobile\UfisMobile\UfisMobile_VS2012\UfisMobile\ufisjs D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\ufisjs

cd C:\Program Files\7-Zip
7z a -tzip D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0.zip D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0\*

move /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0.zip D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0.war

7z a -tzip D:\sandbox\UfisMobile\UfisMobile\UfisMobile.zip D:\sandbox\UfisMobile\UfisMobile\UfisMobile\META-INF D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0.war D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-ejb-1.0.jar

move /y D:\sandbox\UfisMobile\UfisMobile\UfisMobile.zip D:\sandbox\UfisMobile\UfisMobile\UfisMobile.ear
del /f D:\sandbox\UfisMobile\UfisMobile\UfisMobile\UfisMobile-web-1.0.war

pause

