window.UfisMobile = $.extend(true, window.UfisMobile, {
  "config": {
    "navigationType": "slideout",
    "commandMapping": {
      "ios-header-toolbar": {
        "commands": [
          {
            "id": "menu-add",
            "location": "menu"
          },
          {
              "id": "menu-edit",
              "location": "menu"
          },
          {
              "id": "menu-remove",
              "location": "menu"
          }
        ]
      },
      "android-header-toolbar": {
        "commands": [
          {
              "id": "menu-add",
              "location": "menu"
          },
          {
              "id": "menu-edit",
              "location": "menu"
          },
          {
              "id": "menu-remove",
              "location": "menu"
          }
        ]
      },
      "win8-phone-appbar": {
        "commands": [
          {
              "id": "menu-add",
              "location": "menu"
          },
          {
              "id": "menu-edit",
              "location": "menu"
          },
          {
              "id": "menu-remove",
              "location": "menu"
          }
        ]
      },
      "tizen-header-toolbar": {
        "commands": [
          {
              "id": "menu-add",
              "location": "menu"
          },
          {
              "id": "menu-edit",
              "location": "menu"
          },
          {
              "id": "menu-remove",
              "location": "menu"
          }
        ]
      },
      "generic-header-toolbar": {
          "commands": [
            {
                "id": "menu-add",
                "location": "menu"
            },
            {
                "id": "menu-edit",
                "location": "menu"
            },
            {
                "id": "menu-remove",
                "location": "menu"
            }
          ]
      }
    },

    "navigation": [
      {
          "title": "Home",
          "action": "#Home/Home",
          "icon": "home"
      },
      {
          "title": "Filter",
          "action": "#Home/Filter",
          "icon": "find"
      },
      {
          "title": "Refresh",
          "action": "#Home/Refresh",
          "icon": "refresh"
      },
      /*{
          "title": "Settings",
          "action": "#Home/Settings/0",
          "icon": "preferences"
      },*/
      {
          "title": "About",
          "action": "#About/About",
          "icon": "tips"
      },
      {
          "title": "Logout",
          "action": "#Login/Logout",
          "icon": "todo"
      }
    ]
  }
});