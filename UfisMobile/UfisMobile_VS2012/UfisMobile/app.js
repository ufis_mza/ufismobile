﻿window.UfisMobile = window.UfisMobile || {};
$(function() {
    // Uncomment the line below to disable platform-specific look and feel and to use the Generic theme for all devices
    // DevExpress.devices.current({ platform: "generic" });

    document.addEventListener("deviceready", onDeviceReady, false);
    UfisMobile.app = new DevExpress.framework.html.HtmlApplication({
        namespace: UfisMobile,
        commandMapping: UfisMobile.config.commandMapping,
        navigationType: UfisMobile.config.navigationType,
        navigation: getNavigationItems()
    });

    UfisMobile.app.router.register(":view/:action/:id/:adid", { view: "Login", action: undefined, id: undefined, adid:undefined });
    
    function showMenu(e) {
        UfisMobile.app.viewShown.remove(showMenu);

        if (e.viewInfo.viewName !== "Home")
            return;

        setTimeout(function() {
            $(".nav-button").trigger("dxclick");
        }, 1000);
    }
    
    function getNavigationItems() {
        if(DevExpress.devices.current().platform === "win8") {
            UfisMobile.config.navigation.push({
                "title": "Panorama",
                "action": "#Panorama",
                "icon": "favorites"
            },
            {
                "title": "Pivot",
                "action": "#Pivot",
                "icon": "favorites"
            });
        }
        return UfisMobile.config.navigation;
    }

    function onDeviceReady() {
        document.addEventListener("backbutton", onBackButton, false);
    }

    function onBackButton() {
        if (UfisMobile.app.canBack()) {
            UfisMobile.app.back();
        } else {
            switch(DevExpress.devices.current().platform) {
                case "tizen":
                    tizen.application.getCurrentApplication().exit();
                    break;
                case "android":
                    navigator.app.exitApp();
                    break;
                case "win8":
                    window.external.Notify("DevExpress.ExitApp");
                    break;
            }
        }
    }

    UfisMobile.app.viewShown.add(showMenu);
});
