﻿UfisMobile.Login = function (params) {
    var viewModel = {
        strTitle: "UfisMobile",
        bLoginError: ko.observable(false),
        loadPanelVisible: ko.observable(false),
        strUsr: ko.observable(""),
        strPwd: ko.observable(""),
        strErrorInfo: ko.observable("Login fails!"),
        fnLogin: function () {
            viewModel.loadPanelVisible(true);
            var vRet = UfisMobile.ViewsHandler.fnLogin(viewModel.strUsr(), viewModel.strPwd(), viewModel.fnLoginSuccess, viewModel.fnLoginFail);
            viewModel.loadPanelVisible(false);
            if (vRet.length === 0) {
                vLoggedIn = true;
                UfisMobile.app.navigate("Home/Home", { root: true });
            }
            else {
                viewModel.strErrorInfo("Login fails : " + vRet);
                viewModel.bLoginError(true);
            }
        },
        hideLoginErrorOverlay: function () {
            viewModel.bLoginError(false);
        },
        viewShown: function () {
            if (params.action === "Logout") {
                UfisMobile.ViewsHandler.fnLogout(viewModel);                
            }
        }
    };

    return viewModel;
};