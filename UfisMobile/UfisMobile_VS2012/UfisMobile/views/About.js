﻿UfisMobile.About = function (params) {
    var viewModel = {
        strAppInfo: ko.observable(UfisMobile.Constants.AppInfo),
        viewShown: function () {}
    };
    return viewModel;
};