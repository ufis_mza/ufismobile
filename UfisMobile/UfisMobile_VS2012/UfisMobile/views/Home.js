﻿UfisMobile.Home = function (params) {
    var viewModel = {
        bInfoAlert: ko.observable(false),
        bLoading: ko.observable(false),
        strInfo: ko.observable("Getting data fails"),
        fnDataRetrieveError: function (str) {
            if (str !== null && str.trim().length > 0)
                viewModel.strInfo("Getting data fails : " + str);

            viewModel.bInfoAlert(true);
        },
        fnDataRetrieveSuccess: function () {
        },
        hideErrorOverlay: function () {
            viewModel.bInfoAlert(false);
            viewModel.bLoading(false);
        },
        viewShowing: function () {
            viewModel.bLoading(true);
        },
        viewShown: function () {
            UfisMobile.ViewsHandler.fnShow(viewModel, ["#divCellA1", "#divHeader1", "#divCellA2", "#divHeader2", "#divCellB1", "#divData1", "#divCellB2", "#divData2"], params.action, params.id, params.adid);
        }
    };

    return viewModel;
};