﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-10
//Developer     : MZA (Zaw Min Tun)
//Description   : To set up the UI theme / coloring information

(function () {
    var vForeColor = "#ffffff", vForeColorDark = "#000000", vBackColor = "#ffffff", vBackColorCr = "#ff0000", vBackColorDepL = "#7799BB", vBackColorArrL = "#777777", vBackColorArrD = "#555555", vBackColorDepD = "#555777", vUpdatedColor = "#3300FF";

    var vTimingColorMatrix = [
    { "value": "", "color": "" },
    { "value": "A", "color": "#339900" },
    { "value": "U", "color": "#ff0000" },
    { "value": "F", "color": "#9900ff" },
    { "value": "R", "color": "#33ccff" },
    { "value": "M", "color": "#33ccff" }
    ];

    var vFlightScheduleColor = {
        "ArrFtypZ": "#ff0000",
        "ArrTMOA": "#ffff00",
        "ArrLAND": "#339900",
        "ArrONBL": "#666666",
        "DepAIRB": "#cccccc",
        "DepOFBL": "#ffcc33",
        "DepFtypZ": "#ff0000"
    };

    var vFlightSpecialColor = {
        "ArrTMOA": "#ffff00",
        "ArrLAND": "#339900",
        "ArrONBL": "#ffffff",
        "DepOFBL": "#ffffff"
    };

    var vFlightTimingCellBgColor = {
        "ArrONBL": "#ff0000",
        "ArrPUSEX": "#ff0000",
        "DepOFBL": "#ff0000",
        "DepPUSEX": "#ff0000"
    };

    UfisMobile.Theme = {
        ForeColor: vForeColor,
        ForeColorDark: vForeColorDark,
        BackColor: vBackColor,
        BackColorCr: vBackColorCr,
        BackColorDepL: vBackColorDepL,
        BackColorArrL: vBackColorArrL,
        BackColorArrD: vBackColorArrD,
        BackColorDepD: vBackColorDepD,
        UpdatedColor: vUpdatedColor,
        TimingColorMatrix: vTimingColorMatrix,
        FlightScheduleColor: vFlightScheduleColor,
        FlightSpecialColor: vFlightSpecialColor,
        FlightTimingCellBgColor: vFlightTimingCellBgColor
    };
})();