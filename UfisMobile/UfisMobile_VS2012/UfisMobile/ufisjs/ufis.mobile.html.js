﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-20
//Developer     : MZA (Zaw Min Tun)
//Description   : A library to generate html controls 
//Remark        : No corresponding usage currently, some usage and test are commented

(function () {
    var fnGetDropDownListImp = function (vLst, vId, txtFld, vlFld, vStyle, vSelected) {
        var _select;

        if (vStyle === null || vStyle === undefined)
            vStyle = "";

        if (vStyle.length > 0) {
            _select = $('<select id="' + vId + '" style="' + vStyle + '">');
        }
        else
            _select = $('<select id="' + vId + '">');

        var i = 0;

        if (vLst !== null && vLst !== undefined) {
            $.each(vLst, function (i1, r1) {
                if(i===vSelected)
                    _select.append($('<option selected="selected"></option>').val(r1[vlFld]).html(r1[txtFld]));
                else
                    _select.append($('<option></option>').val(r1[vlFld]).html(r1[txtFld]));

                i++;
            });
        }
        return _select;
    };

    var fnAcceptOnlyAtoZImp = function (evt) {
        evt = evt || window.event;
        var charCode = evt.which || evt.keyCode;

        if (charCode === 8 /*BACKSPACE*/ || charCode === 46 /*DELETE*/)
            return true;

        var charStr = String.fromCharCode(charCode);
        if (charStr >= 'A' && charStr <= 'z')
            return true;

        return false;
    };

    var fnTrackChecksImp = function (vChks, vIDStr, vDataToChk) {
        var k, vOther = 0;
        var vTerVal = vChks.attr('value');
        var vChk = vChks.prop('checked');

        if (vTerVal === "All") {
            if (vDataToChk !== null && vDataToChk.length > 0) {
                if (vChk === true) {
                    for (k = 0; k < vDataToChk.length; k++) {
                        if (k === 0) {
                            vDataToChk[k].sel = vChk;
                        }
                        else {
                            vDataToChk[k].sel = false;
                            $("#" + vIDStr + k).prop('checked', false);
                        }
                    }
                }
                else {
                    for (k = 1; k < vDataToChk.length; k++) {
                        if (vDataToChk[k].sel === true) {
                            vOther++;
                        }
                    }

                    if (vOther === 0 || vOther === vDataToChk.length - 1) {
                        for (k = 0; k < vDataToChk.length; k++) {
                            if (k === 0) {
                                vDataToChk[k].sel = true;
                                $("#" + vIDStr + k).prop('checked', true);
                            }
                            else {
                                vDataToChk[k].sel = false;
                                $("#" + vIDStr + k).prop('checked', false);
                            }
                        }
                    }
                    else
                        vDataToChk[0].sel = vChk;
                }
            }
        }
        else if (vTerVal !== "All") {
            if (vDataToChk !== null && vDataToChk.length > 0) {
                //Take the input check into array
                for (k = 0; k < vDataToChk.length; k++) {
                    if (vDataToChk[k].text === vTerVal) {
                        vDataToChk[k].sel = vChk;
                        k = vDataToChk.length + 1;
                    }
                }

                for (k = 1; k < vDataToChk.length; k++) {
                    if (vDataToChk[k].sel === true) {
                        vOther++;
                    }
                }

                if ((vOther === 0 && vDataToChk[0].sel === false) || (vOther === vDataToChk.length - 1)) {
                    for (k = 0; k < vDataToChk.length; k++) {
                        if (k === 0) {
                            vDataToChk[k].sel = true;
                            $("#" + vIDStr + k).prop('checked', true);
                        }
                        else {
                            vDataToChk[k].sel = false;
                            $("#" + vIDStr + k).prop('checked', false);
                        }
                    }
                }
                else {
                    vDataToChk[0].sel = false;
                    $("#" + vIDStr + "0").prop('checked', false);
                }
            }
        }
    };

    //Just to remove the # from the id
    var fnGetIdForJavaScriptImp = function (jQueryId) {
        if (jQueryId === null || jQueryId === undefined)
            return jQueryId;

        jQueryId = jQueryId.trim();
        if (jQueryId.substring(0, 1) === "#")
            return jQueryId.substring(1, jQueryId.length);

        return jQueryId;
    };

    var fnSleepImp = function (ms) {
        var startTime = new Date().getTime(); 
        while (new Date().getTime() < startTime + ms);
    }

    UfisMobile.Html = {
        fnGetDropDownList: fnGetDropDownListImp,
        fnAcceptOnlyAtoZ: fnAcceptOnlyAtoZImp,
        fnTrackChecks: fnTrackChecksImp,
        fnGetIdForJavaScript: fnGetIdForJavaScriptImp,
        fnSleep: fnSleepImp
    };
})();