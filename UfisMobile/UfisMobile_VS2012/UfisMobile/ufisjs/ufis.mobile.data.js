﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-10
//Developer     : MZA (Zaw Min Tun)
//Description   : A library for retrieving data from sources or hard-coded

(function () {
    var vPUSESettingsToFilter = [{ text: "All", status: 'A', sel: true }, { text: "None", status: ' ', sel: false }, { text: "OK", status: 'K', sel: false }, { text: "PENDING", status: 'P', sel: false }, { text: "NO OK", status: 'N', sel: false }];
    var vPreuseSettings = [{ text: "None", status: ' ' }, { text: "OK", status: 'K' }, { text: "PENDING", status: 'P' }, { text: "NO OK", status: 'N' }];
    var vPostuseSettings = [{ text: "None", status: ' ' }, { text: "OK", status: 'K' }, { text: "PENDING", status: 'P' }, { text: "NO OK", status: 'N' }];
    var vADIDSettings = [{ text: "Arrival" }, { text: "Departure" }, { text: "Both" }];
    var vTerminals = [{ text: "All", sel: true, value: "A" }, { text: "North", sel: false, value: "N" }, { text: "MTB", sel: false, value: "M" }, { text: "South", sel: false, value:"S" }];
    var vTimeZones = [{ text: "Local", status: "L" }, { text: "UTC", status: "U" }];
    var vPuseConflict = { "MENOPO": "UMPO", "MENOPR": "UMPR", "METY": "01", "PRIO": "01" };
    
    function fnGetData(strReq, arrIndx1, arrIndx2, vDebugJSON /*This variable is only for debugging*/) {
        var vRet = [{}];
        if (!UfisMobile.Constants.DebugMode) {
            var fnSuccess = function (result) {
                var vL1IndexMapGDSucc = UfisMobile.Core.fnGetIndexMap(arrIndx1, result.BODY.ACTS[0].FLD);
                var vL2IndexMapGDSucc = UfisMobile.Core.fnGetIndexMap(arrIndx2, result.BODY.ACTS[0].DATA[0][vL1IndexMapGDSucc.FLDS]);
                vRet = UfisMobile.Core.fnGetDataset(result.BODY.ACTS[0].DATA, 1, -1, result.BODY.ACTS[0].DATA[0][vL1IndexMapGDSucc.NAME], vL1IndexMapGDSucc.NAME, vL1IndexMapGDSucc.VALU, vL2IndexMapGDSucc);
            };
            var fnFail = function (request, status, error) { UfisMobile.Cache.CurrentError = "Fail to get basic information"; return {};};
            var fnError = function (error) { UfisMobile.Cache.CurrentError = "Error while getting basic information"; return {}; };
            UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", strReq, "json", false, fnSuccess, fnFail, fnError);
        }
        else {
            //For offline development & testing
            var vL1IndexMapOfflineGDSucc = UfisMobile.Core.fnGetIndexMap(arrIndx1, vDebugJSON.BODY.ACTS[0].FLD);
            var vL2IndexMapOfflineGDSucc = UfisMobile.Core.fnGetIndexMap(arrIndx2, vDebugJSON.BODY.ACTS[0].DATA[0][vL1IndexMapOfflineGDSucc.FLDS]);
            vRet = UfisMobile.Core.fnGetDataset(vDebugJSON.BODY.ACTS[0].DATA, 1, -1, vDebugJSON.BODY.ACTS[0].DATA[0][vL1IndexMapOfflineGDSucc.NAME], vL1IndexMapOfflineGDSucc.NAME, vL1IndexMapOfflineGDSucc.VALU, vL2IndexMapOfflineGDSucc);
        }

        return vRet;
    }

    var fnGetALTTABImp = function () {
        if (!(UfisMobile.Cache.ALTTAB === null || UfisMobile.Cache.ALTTAB === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.ALTTAB), "[{}]")))
            return;

        var strReq = UfisMobile.Constants.ALTTABString.replace("_USERNAME", UfisMobile.Cache.CurrentUser);
        var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
        var vALTCols = ["a.URNO", "a.ALC2"];
        UfisMobile.Cache.ALTTAB = fnGetData(strReq, vArr1, vALTCols, UfisMobile.TestData.ALTTAB);
    };

    var fnGetACTTABImp = function () {
        if (!(UfisMobile.Cache.ACTTAB === null || UfisMobile.Cache.ACTTAB === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.ACTTAB), "[{}]")))
            return;

        var strReq = UfisMobile.Constants.ACTTABString.replace("_USERNAME", UfisMobile.Cache.CurrentUser);
        var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
        var vALTCols = ["a.URNO", "a.ACT3", "a.ACT5", "a.ACFN"];
        UfisMobile.Cache.ACTTAB = fnGetData(strReq, vArr1, vALTCols, UfisMobile.TestData.ACTTAB);
    };

    var fnGetHAGTABImp = function () {
        if (!(UfisMobile.Cache.HAGTAB === null || UfisMobile.Cache.HAGTAB === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.HAGTAB), "[{}]")))
            return;

        var strReq = UfisMobile.Constants.HAGTABString.replace("_USERNAME", UfisMobile.Cache.CurrentUser);
        var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
        var vHAGCols = ["a.HSNA", "a.HNAM"];
        UfisMobile.Cache.HAGTAB = fnGetData(strReq, vArr1, vHAGCols, UfisMobile.TestData.HAGTAB);
    };

    var fnGetHAITABImp = function () {
        if (!(UfisMobile.Cache.HAITAB === null || UfisMobile.Cache.HAITAB === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.HAITAB), "[{}]")))
            return;

        var strReq = UfisMobile.Constants.HAITABString.replace("_USERNAME", UfisMobile.Cache.CurrentUser);
        var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
        var vHAICols = ["a.HSNA", "a.ALTU", "a.TASK"];
        var vTblTmp = fnGetData(strReq, vArr1, vHAICols, UfisMobile.TestData.HAITAB);

        //Remove all which is not related to RAMP
        UfisMobile.Cache.HAITAB = [];
        $.each(vTblTmp, function (i, r) {
            if (r.a_TASK !== null && r.a_TASK !== undefined && r.a_TASK.toLowerCase().indexOf("full handl")>=0)//just to take all full handle or full handling or anything similar
                UfisMobile.Cache.HAITAB.push(JSON.parse(JSON.stringify(r)));
        });
    };

    var fnGetHandlersInfoImp = function () {
        if (!(UfisMobile.Cache.HandlersInfo === null || UfisMobile.Cache.HandlersInfo === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.HandlersInfo), "[{}]") || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.HandlersInfo), "[]")))
            return;

        fnGetALTTABImp();
        fnGetHAGTABImp();
        fnGetHAITABImp();
        
        var vT1 = UfisMobile.Core.fnJoinTables(UfisMobile.Cache.HAITAB, "a_HSNA", null, UfisMobile.Cache.HAGTAB, "a_HSNA", null);
        var vColsToGet = ["a_ALC2"];
        UfisMobile.Cache.HandlersInfo = UfisMobile.Core.fnJoinTables(UfisMobile.Cache.ALTTAB, "a_URNO", vColsToGet, vT1, "a_ALTU", null);
    };

    var fnGetParkingStandsImp = function () {
        if (!(UfisMobile.Cache.ParkingStands === null || UfisMobile.Cache.ParkingStands === undefined || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.ParkingStands), "[{}]") || UfisMobile.Core.fnAbsEqual(JSON.stringify(UfisMobile.Cache.ParkingStands), "[]")))
            return;

        var strReq = UfisMobile.Constants.ParkingStandsString.replace("_USERNAME", UfisMobile.Cache.CurrentUser);
        var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
        var vPSCols = ["a.PNAM"];
        UfisMobile.Cache.ParkingStands = fnGetData(strReq, vArr1, vPSCols, UfisMobile.TestData.ParkingStands);
    };

    var fnGetUserColumnImp = function (strCol) {
        //To enhance : use a mapping table 
        if (strCol === "ONBL")
            return "ONBU";
        else if (strCol === "OFBL")
            return "OFBU";

        return strCol;
    };

    var fnGetUTCDImp = function () {
        if (!UfisMobile.Constants.DebugMode) {
            var fnSuccess = function (result) {
                if (result === null || result === undefined) {
                    alert("There was an error in UTC Time offset info!");
                    UfisMobile.Cache.UTCD = 0;
                }
                else {
                    $.each(result.BODY.RESP[0].RECS, function (i1, r1) {
                        if (r1 !== null && r1 !== undefined && r1[0] === "UTCD" && r1.length > 1) {
                            UfisMobile.Cache.UTCD = r1[1] * 1;
                            return false;
                        }
                    });
                }
            };

            var strReq = UfisMobile.Constants.UTCDString;
            var fnFail = function (request, status, error) { UfisMobile.Cache.CurrentError = "Fail to get UTC Time offset"; };
            var fnError = function (error) { UfisMobile.Cache.CurrentError = "Error while getting UTC Time offset"; };
            UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", strReq, "json", false, fnSuccess, fnFail, fnError);
        }
        else {
            var vJsonUTCD = UfisMobile.TestData.UTCDInfo;
            $.each(vJsonUTCD.BODY.RESP[0].RECS, function (i1, r1) {
                if (r1 !== null && r1 !== undefined && r1[0] === "UTCD" && r1.length > 1) {
                    UfisMobile.Cache.UTCD = r1[1] * 1;
                    return false;
                }
            });
        }
    };

    var fnGetErrorImp = function (jsonObj) {
        if (jsonObj.BODY.hasOwnProperty("RESP") && jsonObj.BODY.RESP[0].hasOwnProperty("ERRORLIST") &&
            jsonObj.BODY.RESP[0].ERRORLIST !== null && jsonObj.BODY.RESP[0].ERRORLIST !== undefined && jsonObj.BODY.RESP[0].ERRORLIST.length > 0) {
            UfisMobile.Cache.CurrentError = jsonObj.BODY.RESP[0].ERRORLIST[0];
        }
        else {
            UfisMobile.Cache.CurrentError = "";
        }
    };

    var fnGetPreuseTextImp = function (vPuse) {
        if (vPuse === null || vPuse === undefined)
            return "";

        var vAns = "";
        vPuse = vPuse.trim();
        $.each(UfisMobile.Data.PreuseSettings, function (i, r) {
            if (r.status === vPuse) {
                vAns = r.text;
                return false;
            }
        });

        return vAns;
    };

    var fnGetPostuseTextImp = function (vPuse) {
        if (vPuse === null || vPuse === undefined)
            return "";

        var vAns = "";
        vPuse = vPuse.trim();
        $.each(UfisMobile.Data.PostuseSettings, function (i, r) {
            if (r.status === vPuse) {
                vAns = r.text;
                return false;
            }
        });

        return vAns;
    };

    UfisMobile.Data = {
        PUSESettingsToFilter: vPUSESettingsToFilter,
        PreuseSettings: vPreuseSettings,
        PostuseSettings: vPostuseSettings,
        ADIDSettings: vADIDSettings,
        Terminals: vTerminals,
        TimeZones: vTimeZones,
        PuseConflict: vPuseConflict,
        fnGetACTTAB: fnGetACTTABImp,
        fnGetALTTAB: fnGetALTTABImp,
        fnGetHAGTAB: fnGetHAGTABImp,
        fnGetHAITAB: fnGetHAITABImp,
        fnGetHandlersInfo: fnGetHandlersInfoImp,
        fnGetUserColumn: fnGetUserColumnImp,
        fnGetParkingStands: fnGetParkingStandsImp,
        fnGetUTCD: fnGetUTCDImp,
        fnGetError: fnGetErrorImp,
        fnGetPreuseText: fnGetPreuseTextImp,
        fnGetPostuseText: fnGetPostuseTextImp
    };
})();