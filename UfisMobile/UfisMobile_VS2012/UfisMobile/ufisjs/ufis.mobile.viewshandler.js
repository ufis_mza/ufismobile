﻿(function () {
    var fnLoginImp = function (usr, pwd) {
        var vResult = "";
        if (!UfisMobile.Constants.DebugMode) {
            var fnCheckLoginResult = function (objJson) {
                var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
                var vIndxMap = UfisMobile.Core.fnGetIndexMap(vArr1, objJson.BODY.ACTS[0].FLD);

                if (objJson !== null && objJson.BODY.ACTS[0].DATA && objJson.BODY.ACTS[0].DATA.length >= 3) {                    
                    return "";//Login success
                }
                else if (objJson !== null && objJson.BODY.ACTS[0].DATA && objJson.BODY.ACTS[0].DATA.length > 0 && objJson.BODY.ACTS[0].DATA && objJson.BODY.ACTS[0].DATA.length < 3) {
                    var vLoginResult = objJson.BODY.ACTS[0].DATA[0][vIndxMap.VALU][0];
                    return vLoginResult;
                }

                return "Login fails for unknown reason";
            };

            if (usr === null || usr.trim().length === 0 || pwd === null || pwd.trim().length === 0)
                return "Both username and password must be provided";
            else {
                UfisMobile.Cache.CurrentUser = usr;
                var vParam = UfisMobile.Constants.LoginString.replace("_USERNAME", usr).replace("_USERNAME", usr).replace("_PASSWORD", pwd);
                var fnSuccess = function (result) {
                    UfisMobile.Data.fnGetError(result);
                    vResult = fnCheckLoginResult(result).trim();
                    if (vResult.length === 0) {
                        UfisMobile.Cache.LastUpdate = new Date();
                        fnSetupFlightListRefresh();
                        UfisMobile.Cache.Flights = result;                        
                    }
                };

                var fnFail = function (request, status, error) { vResult = "UNKOWN FAILURE"; };
                var fnError = function (error) { vResult = "UNKOWN ERROR"; };
                UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", vParam, "json", false, fnSuccess, fnFail, fnError);
                return vResult;
            }
        }
        else {
            //For offline development & testing
            UfisMobile.Cache.CurrentUser = "DEBUG";
            UfisMobile.Cache.Flights = UfisMobile.TestData.FlightRecords;
            UfisMobile.Cache.LastUpdate = new Date();
            fnSetupFlightListRefresh();
            return vResult;
        }
    };

    var vFirstTestDataSet = false;
    function fnFlightListGetRefreshData() {
        if (!UfisMobile.Constants.DebugMode) {
            var vParam = UfisMobile.Constants.FlightListString.replace("_SELECT", "").replace("_USERNAME", UfisMobile.Cache.CurrentUser);
            var fnSuccess = function (result) {
                UfisMobile.Data.fnGetError(result);
                UfisMobile.Cache.LastUpdate = new Date();
                UfisMobile.Cache.Flights = result;
            };
            var fnFail = function (request, status, error) { UfisMobile.Cache.CurrentError = "Fail to retrieve flight list"; };
            var fnError = function (error) { UfisMobile.Cache.CurrentError = "Error while retrieving flight list"; };
            UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", vParam, "json", false, fnSuccess, fnFail, fnError);
        }
        else {//For offline development & testing            
            //For checking data automatic refresh/update
            if (vFirstTestDataSet === true) {
                UfisMobile.Cache.Flights = UfisMobile.TestData.FlightRecords2;
                vFirstTestDataSet = false;
            }
            else {
                vFirstTestDataSet = true;
                UfisMobile.Cache.Flights = UfisMobile.TestData.FlightRecords1;
            }
            UfisMobile.Cache.LastUpdate = new Date();
        }
    }
    
    function fnLoadMiscelInfo() {
        UfisMobile.Data.fnGetHandlersInfo();
        UfisMobile.Data.fnGetParkingStands();
        UfisMobile.Data.fnGetUTCD();
        UfisMobile.Data.fnGetACTTAB();

        //Testing Code : UTC <-> Local  
        //var vDtTest = new Date(2014, 1, 1, 10, 1, 1);
        //var vDtRet = UfisMobile.DateTime.fnConvertLocalDateToUTC(vDtTest);
        //var vDtRet1 = UfisMobile.DateTime.fnConvertUTCDateToLocal(vDtRet);
        //alert(UfisMobile.DateTime.fnDateTimeToY2M2D2H2M2S2(vDtTest) + "<<<>>>" + UfisMobile.DateTime.fnDateTimeToY2M2D2H2M2S2(vDtRet) + "<<<>>>" + UfisMobile.DateTime.fnDateTimeToY2M2D2H2M2S2(vDtRet1));
    }

    function fnFlightListGetData() {
        try {
            var vDSRet;

            var vDtNow = new Date();
            var vUpdateElapsed = vDtNow - UfisMobile.Cache.LastUpdate;
            if (vUpdateElapsed > UfisMobile.Cache.RefreshRate) {
                fnFlightListGetRefreshData();
                fnSetupFlightListRefresh();
            }

            if (UfisMobile.Cache.Flights !== null) {
                vDSRet = UfisMobile.Cache.Flights;
                var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
                var vL1IndexMap = UfisMobile.Core.fnGetIndexMap(vArr1, vDSRet.BODY.ACTS[0].FLD);
                var vArr2 = ["a.URNO", "a.FLNO", "a.ADID", "a.STOA", "a.STOD", "a.ORG3", "a.DES3", "a.REM1", "a.PUSE", "a.FTYP", "a.TTYP", "a.OFBL", "a.ONBL", "a.ETAI", "a.ETDI", "a.CSGN", "a.ALC2", "a.REGN", "a.ACT3", "a.PSTD", "a.PSTA", "a.TMOA", "a.CTOT", "a.LAND", "a.AIRB", "a.STEA", "a.STED", "a.STON", "a.STOF", "a.STLD", "a.STAB", "a.STTM", "a.STEV", "a.STE2", "a.STE3", "a.STE4"];
                var vL2IndexMap = UfisMobile.Core.fnGetIndexMap(vArr2, vDSRet.BODY.ACTS[0].DATA[0][vL1IndexMap.FLDS]);
                var vDS = UfisMobile.Core.fnGetDataset(vDSRet.BODY.ACTS[0].DATA, 1, -1, vDSRet.BODY.ACTS[0].DATA[0][vL1IndexMap.NAME], vL1IndexMap.NAME, vL1IndexMap.VALU, vL2IndexMap);
                if (!UfisMobile.Constants.DebugMode)
                    UfisMobile.Cache.RecentFilter = UfisMobile.Application.fnTranslateFilter(UfisMobile.Application.fnGetFilter(UfisMobile.Cache.Flights, vL1IndexMap.VALU));
                else
                    //UfisMobile.Cache.RecentFilter = UfisMobile.Application.fnTranslateFilter("((a.ADID in ('A', 'B') AND a.TIFA > TO_CHAR(JCEDANOW-(2/24)\u0019'YYYYMMDDHH24MISS') AND a.TIFA < TO_CHAR(JCEDANOW+(2/24)\u0019'YYYYMMDDHH24MISS')) or (a.ADID in ('B', 'D') AND a.TIFD > TO_CHAR(JCEDANOW-(2/24)\u0019'YYYYMMDDHH24MISS')  AND a.TIFD < TO_CHAR(JCEDANOW+(2/24)\u0019'YYYYMMDDHH24MISS'))) and (a.PSTA = '33' or a.PSTD = '33') and (a.FLNO LIKE '%7%') and (a.TGA1 in ('N','S')) and (FTYP in ('X','N')) and (TTYP in ('D','I')))");
                    UfisMobile.Cache.RecentFilter = UfisMobile.Application.fnTranslateFilter("(a.ADID in ('A', 'B') AND a.TIFA > TO_CHAR(JCEDANOW-(2/24)\u0019'YYYYMMDDHH24MISS') AND a.TIFA < TO_CHAR(JCEDANOW+(2/24)\u0019'YYYYMMDDHH24MISS')) and (a.PSTA = '33' or a.PSTD = '33') and (a.FLNO LIKE '%7%') and (a.TGA1 in ('N','S')) and (FTYP in ('X','N')) and (TTYP in ('D','I')))");

                var vRet;

                if (UfisMobile.Cache.FlightsRecent === null || UfisMobile.Cache.FlightsRecent === undefined) {
                    UfisMobile.Cache.FlightsRecent = vDS;
                    vRet = {
                        data: vDS
                    };
                } else {
                    vDS = UfisMobile.Core.fnMarkUpdates(UfisMobile.Cache.FlightsRecent, vDS, "a_URNO", ["a_ETAI", "a_ETDI", "a_TMOA", "a_LAND", "a_AIRB", "a_CTOT", "a_ONBL", "a_OFBL"]);
                    vRet = {
                        data: vDS
                    };

                    UfisMobile.Cache.FlightsRecent = vDS;
                }
            }
        } catch (x) {
        }
    }

    function fnFlightListSetLayout(cnts) {
        var vRetArr = "", vRetDep ="";
        var vColsToGet = ["a_HSNA", "a_ALTU", "a_TASK", "a_HNAM"];
        var vDataToShow = UfisMobile.Core.fnJoinTables(UfisMobile.Cache.FlightsRecent, "a_ALC2", null, UfisMobile.Cache.HandlersInfo, "a_ALC2", vColsToGet);
        var vColsToGetACT = ["a_ACFN"];
        vDataToShow = UfisMobile.Core.fnJoinTables(vDataToShow, "a_ACT3", null, UfisMobile.Cache.ACTTAB, "a_ACT3", vColsToGetACT);

        //The sequence of calling fnArrDepStampOnTowingFlights and fnRemoveAirbornFlights is important
        vDataToShow = UfisMobile.Application.fnArrDepStampOnTowingFlights(vDataToShow);
        vDataToShow = UfisMobile.Application.fnRemoveAirborneFlights(vDataToShow);

        var vRcntFltr = UfisMobile.Cache.RecentFilter;
        var vArrDep = "";
        var bNoFlightsArr = false, bNoFlightsDep = false;

        if (vRcntFltr === null || vRcntFltr === undefined)
            vArrDep = "B";
        else if (vRcntFltr.ADID.indexOf("A") >= 0)
            vArrDep = "A";
        else if (vRcntFltr.ADID.indexOf("D") >= 0)
            vArrDep = "D";
        else
            vArrDep = "B";

        if (vArrDep === "A") {
            vRetArr = fnGenerateFlightListLayout("A", vDataToShow);
            $(cnts[5]).children().detach().remove();
            $(vRetArr).appendTo($(cnts[5]));//.dxScrollView();

            $(cnts[0]).width('100%');
            $(cnts[0]).css("float", "none");
            $(cnts[1]).text("Arrival");
            document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
            $(cnts[3]).text("");
            $(cnts[4]).width('100%');
            document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';
        }
        else if (vArrDep === "D") {
            vRetDep = fnGenerateFlightListLayout("D", vDataToShow);
            $(cnts[5]).children().detach().remove();
            $(vRetDep).appendTo($(cnts[5]));//.dxScrollView();

            $(cnts[0]).width('100%');
            $(cnts[0]).css("float", "none");
            $(cnts[1]).text("Departure");
            document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
            $(cnts[3]).text("");
            $(cnts[4]).width('100%');
            document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';
        }
        else {
            vRetArr = fnGenerateFlightListLayout("A", vDataToShow);
            $(cnts[5]).children().detach().remove();
            //$(cnt).find('*').not('#divPopups').remove();            
            if (vRetArr.length < 100 && vRetArr.indexOf("No flights") >= 0)
                bNoFlightsArr = true;
        
            vRetDep = fnGenerateFlightListLayout("D", vDataToShow);
            $(cnts[7]).children().detach().remove();
            //$(cnt).find('*').not('#divPopups').remove();
            if (vRetDep.length < 100 && vRetDep.indexOf("No flights") >= 0)
                bNoFlightsDep = true;
        
            if (bNoFlightsArr && !bNoFlightsDep) {
                $(vRetDep).appendTo($(cnts[5]));//.dxScrollView();

                $(cnts[0]).width('100%');
                $(cnts[0]).css("float", "none");
                $(cnts[1]).text("Departure Only");
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
                $(cnts[3]).text("");
                $(cnts[4]).width('100%');
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';
            }
            else if (!bNoFlightsArr && bNoFlightsDep) {
                $(vRetArr).appendTo($(cnts[5]));//.dxScrollView();

                $(cnts[0]).width('100%');
                $(cnts[0]).css("float", "none");
                $(cnts[1]).text("Arrival Only");
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
                $(cnts[3]).text("");
                $(cnts[4]).width('100%');
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';
            }
            else {
                $(vRetArr).appendTo($(cnts[5]));//.dxScrollView();
                $(vRetDep).appendTo($(cnts[7]));//.dxScrollView();
                $(cnts[0]).width('50%');
                $(cnts[0]).css("float", "left");
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '50%';
                $(cnts[4]).width('50%');
                document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '50%';
                $(cnts[1]).text("Arrival");
                $(cnts[3]).text("Departure");
            }
        }
    }

    function fnGenerateFlightListLayout(ArrDep, vDataToShow) {
        var vBackColor = UfisMobile.Theme.BackColor, vBackColorArrD = UfisMobile.Theme.BackColorArrD, vForeColor = UfisMobile.Theme.ForeColor;
        var vScheduleColor = UfisMobile.Theme.BackColor, vSpecialColor = UfisMobile.Theme.BackColor, vOBLBkColor = UfisMobile.Theme.BackColor, vPUSEBkColor = UfisMobile.Theme.BackColor;
        var vTabLineColor = UfisMobile.Theme.TableLineColor, vBackColorCr = UfisMobile.Theme.BackColorCr, vBackColorDepD = UfisMobile.Theme.BackColorDepD;
        var vSt = "<div><ul>";
        var vEd = "</ul></div>";
        var i;
        var vRet = "";
        var vInfo1, vInfo2;
        var vTmp = "";//Use this vairable only after immediate assigning

        var bNoFlights = true;

        if (vDataToShow !== null && vDataToShow.length > 0) {            
            for (i = 0; i < vDataToShow.length; i++) {
                vBackColor = UfisMobile.Theme.BackColor;

                if (ArrDep === "A" && vDataToShow[i]["a_ADID"].indexOf("A") < 0)
                    continue;

                if (ArrDep === "D" && vDataToShow[i]["a_ADID"].indexOf("D") < 0)
                    continue;

                bNoFlights = false;

                vInfo1 = "";
                vInfo2 = "";

                //Data layout
                //Arrival: FLT NUMBER/CALLSIGN/REG/AC TYPE/STAND/ETA/TMO/ATA /NATURE / HANDLER 
                //Departure: FLT NUMBER/CALLSIGN/REG/AC TYPE/STAND/ETD*/ATD/CTOT* NATURE / HANDLER

                vScheduleColor = UfisMobile.Application.fnGetScheduleColor(vDataToShow[i]["a_ADID"], vDataToShow[i]["a_FTYP"], vDataToShow[i]["a_ONBL"], vDataToShow[i]["a_OFBL"], vDataToShow[i]["a_TMOA"], vDataToShow[i]["a_LAND"], vDataToShow[i]["a_AIRB"]);
                vSpecialColor = UfisMobile.Application.fnGetSpecialColor(vDataToShow[i]["a_ADID"], vDataToShow[i]["a_PUSE"], vDataToShow[i]["a_ONBL"], vDataToShow[i]["a_OFBL"], vDataToShow[i]["a_TMOA"], vDataToShow[i]["a_LAND"], vDataToShow[i]["a_AIRB"]);

                if (vSpecialColor.length > 0)
                    vBackColor = vSpecialColor;

                vOBLBkColor = UfisMobile.Application.fnGetCellBgColor(vBackColor, vDataToShow[i]["a_ADID"], vDataToShow[i]["a_ONBL"], vDataToShow[i]["a_OFBL"], null);
                if (vOBLBkColor.length === 0)
                    vOBLBkColor = vBackColor;

                vPUSEBkColor = UfisMobile.Application.fnGetCellBgColor(vBackColor, vDataToShow[i]["a_ADID"], null, null, vDataToShow[i]["a_PUSE"]);
                if (vPUSEBkColor.length === 0)
                    vPUSEBkColor = vBackColor;

                //Commented: Both arrival and departure flights will be displayed in same color
                //if (vBackColor.length === 0) {
                //    if (vDataToShow[i]["a_ADID"].indexOf("A") >= 0)
                //        vBackColor = UfisMobile.Theme.BackColor;
                //    else
                //        vBackColor = UfisMobile.Theme.BackColorDepL;
                //}

                if (vBackColor === "#ffff00" || vBackColor === "#00ff00" || vBackColor === "#ffffff")
                    vForeColor = UfisMobile.Theme.ForeColorDark;
                else
                    vForeColor = UfisMobile.Theme.ForeColor;

                vInfo1 = UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_FLNO"], UfisMobile.Core.fnGetDashString(5, vBackColor)) + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_CSGN"], UfisMobile.Core.fnGetDashString(5, vBackColor), true);
                vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_REGN"], UfisMobile.Core.fnGetDashString(5, vBackColor), true);
                vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_ACT3"], UfisMobile.Core.fnGetDashString(5, vBackColor), true);

                if (vDataToShow[i]["a_ADID"].indexOf("A") >= 0) {
                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_PSTA"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    if (UfisMobile.Cache.CurrentTimeZone === 'L') {
                        if ($.inArray("a_ETAI", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ETAI"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STEA"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ETAI"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STEA"], "")));

                        vInfo1 = vInfo1 + "<br>ETA:" + vTmp;

                        if ($.inArray("a_ONBL", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ONBL"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ONBL"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor);

                        vInfo1 = vInfo1 + " ONB:" + vTmp;

                        if ($.inArray("a_TMOA", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_TMOA"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STOM"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_TMOA"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STOM"], "")));

                        vInfo1 = vInfo1 + " TMO:" + vTmp;

                        if ($.inArray("a_LAND", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_LAND"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STLD"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_LAND"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STLD"], "")));

                        vInfo1 = vInfo1 + " ATA:" + vTmp;
                    }
                    else {
                        if ($.inArray("a_ETAI", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ETAI"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STEA"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ETAI"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STEA"], "")));

                        vInfo1 = vInfo1 + "<br>ETA:" + vTmp;

                        if ($.inArray("a_ONBL", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ONBL"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ONBL"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor);

                        vInfo1 = vInfo1 + " ONB:" + vTmp;

                        if ($.inArray("a_TMOA", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_TMOA"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STOM"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_TMOA"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STOM"], "")));

                        vInfo1 = vInfo1 + " TMO:" + vTmp;

                        if ($.inArray("a_LAND", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_LAND"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STLD"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_LAND"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STLD"], "")));

                        vInfo1 = vInfo1 + " ATA:" + vTmp;
                    }

                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_FTYP"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_TTYP"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    vInfo1 = vInfo1 + "<br>Pre-use: " + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Data.fnGetPreuseText(vDataToShow[i]["a_PUSE"]), UfisMobile.Core.fnGetDashString(2, vBackColor), true, vPUSEBkColor);
                    vInfo1 = vInfo1 + "   Handler: " + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_HNAM"], UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_TASK"], "", true), true), UfisMobile.Core.fnGetDashString(5, vBackColor), true);
                    vInfo2 = "REMARK: <b>" + UfisMobile.Core.fnGetValueSafe(vDataToShow[i]["a_REM1"], "", true) + "</b>";
                }
                else {
                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_PSTD"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    if (UfisMobile.Cache.CurrentTimeZone === 'L') {
                        if ($.inArray("a_ETDI", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ETDI"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STED"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_ETDI"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STED"], "")));

                        vInfo1 = vInfo1 + "<br>ETD:" + vTmp;

                        if ($.inArray("a_OFBL", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_OFBL"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_OFBL"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor);

                        vInfo1 = vInfo1 + " OFB:" + vTmp;

                        if ($.inArray("a_AIRB", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_AIRB"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STAB"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow[i]["a_AIRB"])), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STAB"], "")));

                        vInfo1 = vInfo1 + " ATD:" + vTmp;

                        if ($.inArray("a_CTOT", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_CTOT"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_CTOT"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true);

                        vInfo1 = vInfo1 + " CTOT:" + vTmp;
                    }
                    else {
                        if ($.inArray("a_ETDI", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ETDI"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STED"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_ETDI"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STED"], "")));

                        vInfo1 = vInfo1 + "<br>ETD:" + vTmp;

                        if ($.inArray("a_OFBL", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_OFBL"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_OFBL"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, vOBLBkColor);

                        vInfo1 = vInfo1 + " OFB:" + vTmp;

                        if ($.inArray("a_AIRB", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_AIRB"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STAB"], ""))) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_AIRB"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true, UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_STAB"], "")));

                        vInfo1 = vInfo1 + " ATD:" + vTmp;

                        if ($.inArray("a_CTOT", vDataToShow[i]["UPDATED"]) >= 0)
                            vTmp = "<span style='font-weight:bold;color:" + UfisMobile.Theme.UpdatedColor + "'>" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_CTOT"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true) + "</span>";
                        else
                            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataToShow[i]["a_CTOT"]), UfisMobile.Core.fnGetDashString(5, vBackColor), true);

                        vInfo1 = vInfo1 + " CTOT:" + vTmp;
                    }

                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_FTYP"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    vInfo1 = vInfo1 + " " + UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_TTYP"], UfisMobile.Core.fnGetDashString(2, vBackColor), true);
                    vInfo1 = vInfo1 + "<br>Post-use: " + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Data.fnGetPostuseText(vDataToShow[i]["a_PUSE"]), UfisMobile.Core.fnGetDashString(2, vBackColor), true, vPUSEBkColor);
                    vInfo1 = vInfo1 + "   Handler: " + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_HNAM"], UfisMobile.Core.fnGetValueNonblank(vDataToShow[i]["a_TASK"], "", true), true), UfisMobile.Core.fnGetDashString(5, vBackColor), true);
                    vInfo2 = "REMARK: <b>" + UfisMobile.Core.fnGetValueSafe(vDataToShow[i]["a_REM1"], "", true) + "</b>";
                }

                vRet = vRet + "<li style=\"border:2px solid #8899aa;color:" + vForeColor + ";\" onClick='UfisMobile.app.navigate(\"Home/Detail/" + vDataToShow[i]["a_URNO"] + "/" + vDataToShow[i]["a_ADID"] + "\", { root: true });'>";

                vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%;height:30%\">";//<TABLE-MAIN
                vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;" + vTabLineColor + "\">";//<TABLE-MAIN-ROW1

                if (vDataToShow[i]["a_ADID"].indexOf("A") >= 0) {
                    if (UfisMobile.Application.fnIsCrFlight(vDataToShow[i]) === true) {
                        vRet = vRet + "<div style=\"display: table-cell;border-right:solid 3px " + vBackColorArrD + ";width:2%;background-color:" + vBackColorCr + ";\">";//<TABLE-MAIN-ROW1-CELL1
                        vRet = vRet + "<body><img src=\"images/cr_arrival.png\" style=\"vertical-align:top\" /></body>";
                    }
                    else {
                        vRet = vRet + "<div style=\"display: table-cell;border-right:solid 3px " + vBackColorArrD + ";width:2%;background-color:" + vScheduleColor + ";\">";//<TABLE-MAIN-ROW1-CELL1
                        vRet = vRet + "<body><img src=\"images/arrival.png\" style=\"vertical-align:top\" /></body>";
                    }
                }
                else {
                    if (UfisMobile.Application.fnIsCrFlight(vDataToShow[i]) === true) {
                        vRet = vRet + "<div style=\"display: table-cell;border-right:solid 3px " + vBackColorDepD + ";width:2%;background-color:" + vBackColorCr + ";\">";//<TABLE-MAIN-ROW1-CELL1
                        vRet = vRet + "<body><img src=\"images/cr_departure.png\" style=\"vertical-align:top\" /></body>";
                    }
                    else {
                        vRet = vRet + "<div style=\"display: table-cell;border-right:solid 3px " + vBackColorDepD + ";width:2%;background-color:" + vScheduleColor + ";\">";//<TABLE-MAIN-ROW1-CELL1
                        vRet = vRet + "<body><img src=\"images/departure.png\" style=\"vertical-align:top\" /></body>";
                    }
                }

                vRet = vRet + "</div>";//TABLE-MAIN-ROW1-CELL1>
                vRet = vRet + "<div style=\"display: table-cell;width:98%;background-color:" + vBackColor + ";\">";//<TABLE-MAIN-ROW1-CELL2
                vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%;\">";//<TABLE-RIGHT
                vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;" + vTabLineColor + "\">";//<TABLE-RIGHT-ROW1
                vRet = vRet + "<div style=\"display: table-cell;width:2%;background-color:" + vBackColor + ";\">" + vInfo1;//<TABLE-MAIN-ROW1-CELL1
                vRet = vRet + "</div>";//TABLE-MAIN-ROW1-CELL1>
                vRet = vRet + "</div>";//TABLE-RIGHT-ROW1>
                vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;" + vTabLineColor + "\">";//<TABLE-RIGHT-ROW2
                vRet = vRet + "<div style=\"display: table-cell;width:2%;background-color:" + vBackColor + ";\">" + vInfo2;//<TABLE-MAIN-ROW2-CELL1
                vRet = vRet + "</div>";//TABLE-MAIN-ROW2-CELL1>
                vRet = vRet + "</div>";//TABLE-RIGHT-ROW2>
                vRet = vRet + "</div>";//TABLE-RIGHT>
                vRet = vRet + "</div>";//TABLE-MAIN-ROW1-CELL2>
                vRet = vRet + "</div>";//TABLE-MAIN-ROW1>
                vRet = vRet + "</div>";//TABLE-MAIN>               

                vRet = vRet + "</li>";
            }
        }

        if(bNoFlights)
        {
            vRet = vRet + "<li><div>No flights</div></li>";
        }

        vRet = vSt + vRet + vEd;

        return vRet;
    }   

    function fnSingleFlightGetData(id) {
        try {
            if (!UfisMobile.Constants.DebugMode) {
                var vParam = UfisMobile.Constants.FlightQueryString.replace("_URNO", id).replace("_USERNAME", UfisMobile.Cache.CurrentUser);
                var fnSuccess = function (result) {
                    UfisMobile.Data.fnGetError(result);
                    var vArr1SF = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
                    var vL1IndexMapSF = UfisMobile.Core.fnGetIndexMap(vArr1SF, result.BODY.ACTS[0].FLD);
                    var vArr2SF = ["a.URNO", "a.FLNO", "a.ADID", "a.STOA", "a.STOD", "a.ORG3", "a.DES3", "a.REM1", "a.PUSE", "a.FTYP", "a.TTYP", "a.OFBL", "a.ONBL", "a.ETAI", "a.ETDI", "a.CSGN", "a.ALC2", "a.REGN", "a.ACT3", "a.PSTD", "a.PSTA", "a.TMOA", "a.CTOT", "a.LAND", "a.AIRB", "a.STEA", "a.STED", "a.STON", "a.STOF", "a.STLD", "a.STAB", "a.STTM"];
                    var vL3IndexMapSF = UfisMobile.Core.fnGetIndexMap(vArr2SF, result.BODY.ACTS[0].DATA[0][vL1IndexMapSF.FLDS]);
                    var vRetSF = null;
                    var vRecsSF = UfisMobile.Core.fnGetDataset(result.BODY.ACTS[0].DATA, 1, 1, "", -1, vL1IndexMapSF.VALU, vL3IndexMapSF);
                    if (vRecsSF !== null && vRecsSF.length > 0)
                        vRetSF = vRecsSF[0];

                    UfisMobile.Cache.FlightRecent = vRetSF;
                };

                var fnFail = function (request, status, error) {  UfisMobile.Cache.CurrentError = "Fail to retrieve flight information"; UfisMobile.Cache.FlightRecent = null; };
                var fnError = function (error) { UfisMobile.Cache.CurrentError = "Error while retrieving flight information"; UfisMobile.Cache.FlightRecent = null; };
                UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", vParam, "json", false, fnSuccess, fnFail, fnError);
            }
            else {
                //For offline development & testing
                var vJSON = UfisMobile.TestData.FlightRecord;
                var vArr1 = ["URNO", "ROLE", "GRPN", "NAME", "ACTN", "TABN", "FLDS", "VALU", "TYPE", "UALL", "ULAY", "UPAS", "UFAL"];
                var vL1IndexMap = UfisMobile.Core.fnGetIndexMap(vArr1, vJSON.BODY.ACTS[0].FLD);
                var vArr2 = ["a.URNO", "a.FLNO", "a.ADID", "a.STOA", "a.STOD", "a.ORG3", "a.DES3", "a.REM1", "a.PUSE", "a.FTYP", "a.TTYP", "a.OFBL", "a.ONBL", "a.ETAI", "a.ETDI", "a.CSGN", "a.ALC2", "a.REGN", "a.ACT3", "a.PSTD", "a.PSTA", "a.TMOA", "a.CTOT", "a.LAND", "a.AIRB", "a.STEA", "a.STED", "a.STON", "a.STOF", "a.STLD", "a.STAB", "a.STTM"];
                var vL3IndexMap = UfisMobile.Core.fnGetIndexMap(vArr2, vJSON.BODY.ACTS[0].DATA[0][vL1IndexMap.FLDS]);
                var vRet = null;
                var vRecs = UfisMobile.Core.fnGetDataset(vJSON.BODY.ACTS[0].DATA, 1, 1, "", -1, vL1IndexMap.VALU, vL3IndexMap);
                if (vRecs !== null && vRecs.length > 0)
                    vRet = vRecs[0];

                UfisMobile.Cache.FlightRecent = vRet;
            }
        }
        catch (x) {
        }
    }

    function fnSingleFlightSetLayout(cnt, adidToUse) {
        var vData = [];
        vData.push(UfisMobile.Cache.FlightRecent);
        var vColsToGet = ["a_HSNA", "a_ALTU", "a_TASK", "a_HNAM"];
        var vJoinedInfo = UfisMobile.Core.fnJoinTables(vData, "a_ALC2", null, UfisMobile.Cache.HandlersInfo, "a_ALC2", vColsToGet);
        var vColsToGetACT = ["a_ACFN"];
        vJoinedInfo = UfisMobile.Core.fnJoinTables(vJoinedInfo, "a_ACT3", null, UfisMobile.Cache.ACTTAB, "a_ACT3", vColsToGetACT);

        var vDataToShow = vJoinedInfo[0];

        vDataToShow["a_ADID"] = adidToUse;

        var vDataOriginal = { "URNO": "", "ONBL": "", "OFBL": "", "REM1": "", "ADID": "", "PUSE": "", "STOA": "", "STOD": "" };

        var fnSave = function (vDataOrg) {
            var vDataNew = { "URNO": "", "ONBL": "", "OFBL": "", "REM1": "", "ADID": "", "PUSE": "", "STOA": "", "STOD": "" };
            vDataNew.ADID = vDataOrg.ADID;
            vDataNew.URNO = vDataOrg.URNO;
            vDataNew.STOA = vDataOrg.STOA;
            vDataNew.STOD = vDataOrg.STOD;

            var vDataOCpy = JSON.parse(JSON.stringify(vDataOrg));
            var vDataNCpy = JSON.parse(JSON.stringify(vDataNew));

            var vSEL = "URNO='" + vDataOCpy.URNO + "'";
            var vRem = "";

            if (vDataOCpy.ADID.indexOf("A")>=0)
                vRem = $("#txtRem").val().trim();

            var vDtBl = $("#txtBl").val();

            if (vDataNCpy.ADID.indexOf("A")>=0)
                vDtBl = UfisMobile.DateTime.fnConjugateDateTimeString(vDataNCpy.STOA, vDtBl);
            else if (vDataNCpy.ADID.indexOf("D")>=0)
                vDtBl = UfisMobile.DateTime.fnConjugateDateTimeString(vDataNCpy.STOD, vDtBl);

            if (vDtBl.length === 14) {
                if (UfisMobile.Cache.CurrentTimeZone === 'L')
                    vDtBl = UfisMobile.DateTime.fnLocalDateTimeStringToUfisDateTimeString(vDtBl);

                if (vDataNCpy.ADID.indexOf("A")>=0) {
                    vDataNCpy.ONBL = vDtBl;
                    if (UfisMobile.Cache.CurrentTimeZone === 'L')
                        vDataOCpy.ONBL = UfisMobile.DateTime.fnLocalDateTimeStringToUfisDateTimeString(vDataOCpy.ONBL);
                }
                else if (vDataNCpy.ADID.indexOf("D")>=0) {
                    vDataNCpy.OFBL = vDtBl;
                    if (UfisMobile.Cache.CurrentTimeZone === 'L')
                        vDataOCpy.OFBL = UfisMobile.DateTime.fnLocalDateTimeStringToUfisDateTimeString(vDataOCpy.OFBL);
                }
            } else if (vDtBl.length === 0) {
                if (vDataNCpy.ADID.indexOf("A")>=0) {
                    if(vDataOCpy.ONBL.length>0)
                        vDataNCpy.ONBL = " ";
                }
                else if (vDataNCpy.ADID.indexOf("D")>0) {
                    if (vDataOCpy.OFBL.length > 0)
                        vDataNCpy.OFBL = " ";
                }
            }

            var dxrgPuse = $("#divPuse").dxRadioGroup("instance");
            var vPuse = dxrgPuse.option('value');

            vDataNCpy.REM1 = vRem;
            vDataNCpy.PUSE = vPuse.status;

            var vSqlUpd = UfisMobile.Core.fnGetUpdateSQL(vDataOCpy, vDataNCpy, "\\u001C");

            var vFlds2, vVals2;
            var vParam = UfisMobile.Constants.FlightUpdateString.replace("_SELECT", vSEL).replace("_FIELDS", vSqlUpd.FIELDS).replace("_VALUES", vSqlUpd.VALUES).replace("_USERNAME", UfisMobile.Cache.CurrentUser);
            if (vDataNCpy.PUSE === 'N') {
                vParam = UfisMobile.Constants.FlightUpdateStringWithAttnPuse.replace("_SELECT", vSEL).replace("_FIELDS1", vSqlUpd.FIELDS).replace("_VALUES1", vSqlUpd.VALUES).replace("_USERNAME", UfisMobile.Cache.CurrentUser);
                vFlds2 = "AKTI\\u001CMENO\\u001CMETY\\u001CPRIO\\u001CRURN";
                if (vDataNCpy.ADID.indexOf("A") >= 0) {
                    vVals2 = "JCEDANOW\\u001C" + UfisMobile.Data.PuseConflict.MENOPR + "\\u001C" + UfisMobile.Data.PuseConflict.METY + "\\u001C" + UfisMobile.Data.PuseConflict.PRIO + "\\u001C" + vDataNCpy.URNO;
                }
                else {
                    vVals2 = "JCEDANOW\\u001C" + UfisMobile.Data.PuseConflict.MENOPO + "\\u001C" + UfisMobile.Data.PuseConflict.METY + "\\u001C" + UfisMobile.Data.PuseConflict.PRIO + "\\u001C" + vDataNCpy.URNO;
                }

                vParam = vParam.replace("_FIELDS2", vFlds2).replace("_VALUES2", vVals2);
            }

            var fnSuccess = function (result) {
                UfisMobile.Data.fnGetError(result);
                UfisMobile.Html.fnSleep(UfisMobile.Constants.SaveDelay);
                UfisMobile.app.navigate("Home/Refresh", { root: true });
            };
            var fnFail = function (request, status, error) { UfisMobile.Cache.CurrentError = "Fail to update the flight information"; };
            var fnError = function (error) { UfisMobile.Cache.CurrentError = "Fail while updating the flight information"; };

            UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", vParam, "json", false, fnSuccess, fnFail, fnError);
        };

        var fnCancel = function () {
            UfisMobile.app.navigate("Home/Home", { root: true });
        };

        var fnClear = function () {
            $("#txtBl").val("");
            var vPuse = $("#divPuse").dxRadioGroup("instance");
            vPuse.option('selectedIndex', 0);//Hard-coded
            $("#txtRem").val("");            
        };

        //-1 -> Not OK to onblock, -2 -> Not OK to offblock, 1 -> OK to onblock, 2 -> OK to offblock (To make a more meaningful alert when the user click NOW button)
        var vOnOffBlockable = 0;
        var vColorBack = UfisMobile.Theme.BackColorArrD, vColorBack2 = UfisMobile.Theme.BackColorArrL, vColorFore = UfisMobile.Theme.ForeColor;
        var vRet = "<div style=\"display: table;border: solid 0px;width:100%; color:" + vColorFore + "\">";
        var vTmp = ""; //Use this variable only after assigning with something

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">Flight No </div>";
        vRet = vRet + "<div style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">" + vDataToShow["a_FLNO"] + "</div>";
        vRet = vRet + "</div>";

        vDataOriginal.URNO = vDataToShow["a_URNO"];
        if (vDataToShow["a_ADID"].indexOf("A") >= 0) {
            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_LAND"]), "", true);
            if (vTmp.length > 0)
                vOnOffBlockable = 1;
            else
                vOnOffBlockable = -1;
        
            vDataOriginal.ADID = vDataToShow["a_ADID"];
            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">STOA </div>";
            vRet = vRet + "<div style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
            if (UfisMobile.Cache.CurrentTimeZone === 'L') {
                vDataOriginal.ONBL = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_ONBL"]),"", true);
                vDataOriginal.STOA = UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_STOA"]);
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_STOA"])), UfisMobile.Core.fnGetDashString(2, vColorBack), true) + "</div>";
            }
            else {
                vDataOriginal.ONBL = UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_ONBL"],"", true);
                vDataOriginal.STOA = UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_STOA"], "", true);
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(vDataToShow["a_STOA"]), UfisMobile.Core.fnGetDashString(2, vColorBack), true) + "</div>";
            }

            vRet = vRet + "</div>";
            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">ETAI </div>";
            vRet = vRet + "<div style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
            if (UfisMobile.Cache.CurrentTimeZone === 'L')
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_ETAI"])), UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
            else {
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(vDataToShow["a_ETAI"]), UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
            }
            vRet = vRet + "</div>";

            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">";
            //No need to color in detail screen
            //vRet = vRet + "<span style='background-color:" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_STON"], "")), vColorBack) + "'>ONBL(HH:MM[+D])</span></div>";
            vRet = vRet + "ONBL(HH:MM[+D])</div>";
            vRet = vRet + "<div id=\"divBlDate\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
            vRet = vRet + "</div>";
        }
        else {
            vTmp = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_AIRB"]), "", true);
            if (vTmp.length === 0)
                vOnOffBlockable = 2;
            else
                vOnOffBlockable = -2;
            
            vDataOriginal.ADID = vDataToShow["a_ADID"];

            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">STOD </div>";
            vRet = vRet + "<div style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
            if (UfisMobile.Cache.CurrentTimeZone === 'L') {
                vDataOriginal.OFBL = UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_OFBL"]);
                vDataOriginal.STOD = UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_STOD"]);
                vRet = vRet + UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_STOD"])) + "</div>";
            }
            else {
                vDataOriginal.OFBL = UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_OFBL"], "", true);
                vDataOriginal.STOD = UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_STOD"], "", true);
                vRet = vRet + UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(vDataToShow["a_STOD"]) + "</div>";
            }
            vRet = vRet + "</div>";

            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">ETDI</div>";
            vRet = vRet + "<div style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
            if (UfisMobile.Cache.CurrentTimeZone === 'L')
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(UfisMobile.DateTime.fnUfisDateTimeStringToLocalDateTimeString(vDataToShow["a_ETDI"])), UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
            else
                vRet = vRet + UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMMSlashDay(vDataToShow["a_ETDI"]), UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
                
            vRet = vRet + "</div>";

            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">";
            //No need to color in detail screen
            //vRet = vRet + "<span style='background-color:" + UfisMobile.Core.fnGetValueNonblank(UfisMobile.Application.fnGetTimingColor(UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_STOF"], "")), vColorBack) + "'>OFBL(HH:MM[+D])</span></div>";
            vRet = vRet + "OFBL(HH:MM[+D])</div>";
            vRet = vRet + "<div id=\"divBlDate\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
            vRet = vRet + "</div>";
        }

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        if (vDataToShow["a_ADID"].indexOf("A") >= 0) {
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Pre-use </div>";
        }
        else {
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Post-use </div>";
        }

        vRet = vRet + "<div id=\"subCnt1\" style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;color:#00ff00;background-color:" + vColorBack2 + ";\"></div>";
        vRet = vRet + "</div>";

        if (vDataToShow["a_ADID"].indexOf("A") >= 0) {
            vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
            vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Remark </div>";
            vRet = vRet + "<div id=\"divRemark\" style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
            vRet = vRet + "</div>";
            vRet = vRet + "</div>";
        }

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">REGN </div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
        vRet = vRet + UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_REGN"], UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">CSGN </div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
        vRet = vRet + UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_CSGN"], UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">AC Type </div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
        vRet = vRet + UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_ACFN"], "", true) + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">FTYP </div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
        vRet = vRet + UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_FTYP"], "", true) + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Nature Code </div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";\">";
        vRet = vRet + UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_TTYP"], "", true) + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";\">Handler </div>";
        vRet = vRet + "<div id=\"divDate\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";

        //Show the HNAM if exist
        //If not, show the TASK
        //vRet = vRet + UfisMobile.Core.fnGetValueSafe(vDataToShow["a_HNAM"], "", true) + "</div>";
        vRet = vRet +  UfisMobile.Core.fnGetValueNonblank(UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_HNAM"], UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_TASK"], "", true), true), UfisMobile.Core.fnGetDashString(2, vColorBack2), true) + "</div>"; 
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack + ";\">";
        vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%; color:" + vColorBack + "\">";
        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;\">";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div id=\"divClear\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "<div id=\"divOK\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "<div id=\"divCancel\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "</div>";
        $(cnt).find('*').not('#divPopups').remove();
        $(cnt).append($(vRet));

        var fnNow = function (btn) {
            var vPos;
            if ($("#txtBl").prop('disabled') === false) {
                if (btn.attr('value').trim() === "NOW") {
                    if (UfisMobile.Cache.CurrentTimeZone === 'L')
                        $("#txtBl").val(UfisMobile.DateTime.fnGetLocalNowString("HH:MM"));
                    else
                        $("#txtBl").val(UfisMobile.DateTime.fnGetUTCNowString("HH:MM"));
                }
                else if (btn.attr('value').trim() === "+") {
                    vPos = document.getElementById('txtBl').selectionStart;
                    $("#txtBl").val(UfisMobile.DateTime.fnIncDateTimeString($("#txtBl").val(), "", vPos));
                    UfisMobile.Core.fnSetCaretPosition("txtBl", vPos);
                }
                else if (btn.attr('value').trim() === "-") {
                    vPos = document.getElementById('txtBl').selectionStart;
                    $("#txtBl").val(UfisMobile.DateTime.fnDecDateTimeString($("#txtBl").val(), "", vPos));
                    UfisMobile.Core.fnSetCaretPosition("txtBl", vPos);
                }
            } else {
                if(vOnOffBlockable === -1)
                    alert("Not allowed before Landing");
                else if(vOnOffBlockable === -2)
                    alert("Not allowed after Airborne");
                else
                    alert("Not allowed");
            }
        };

        var fnCheckTimeInput = function () {
            var vIn = $("#txtBl").val();
            var vCurPos = document.getElementById('txtBl').selectionStart;
            var vOut = UfisMobile.DateTime.fnCheckTime(vIn, "", vCurPos);
            $("#txtBl").val(vOut);
        };

        var vDtTmString = ""
        var vDt1, vDt2, vDayDiff = "";
        if (vDataToShow["a_ADID"].indexOf("A") >= 0) {
            vDtTmString = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataOriginal.ONBL), "", true);
            if (vDtTmString.length > 0) {
                vDt1 = UfisMobile.DateTime.fnY4M2D2H2M2S2ToDateTime(vDataOriginal.ONBL);
                vDt2 = UfisMobile.DateTime.fnY4M2D2H2M2S2ToDateTime(vDataOriginal.STOA);
                vDayDiff = vDt1.getDate() - vDt2.getDate();
            }

            if (vDayDiff > 0)
                vDtTmString = vDtTmString + "+" + vDayDiff;
            else if (vDayDiff < 0)
                vDtTmString = vDtTmString + vDayDiff;
        }
        else if (vDataToShow["a_ADID"].indexOf("D") >= 0) {
            vDtTmString = UfisMobile.Core.fnGetValueNonblank(UfisMobile.DateTime.fnGetFormattedDateTimeHHMM(vDataOriginal.OFBL), "")
            if (vDtTmString.length > 0) {
                vDt1 = UfisMobile.DateTime.fnY4M2D2H2M2S2ToDateTime(vDataOriginal.OFBL);
                vDt2 = UfisMobile.DateTime.fnY4M2D2H2M2S2ToDateTime(vDataOriginal.STOD);
                vDayDiff = vDt1.getDate() - vDt2.getDate();

                if (vDayDiff > 0)
                    vDtTmString = vDtTmString + "+" + vDayDiff;
                else if (vDayDiff < 0)
                    vDtTmString = vDtTmString + vDayDiff;
            }
        }
        
        var bOnOffBlockable = !(vOnOffBlockable > 0);
        var divBlDateContainer = $('<div>');
        $('<input />', { type: 'text', id: 'txtBl', style: 'vertical-align:middle;width:100px;height:30px;font-size:25px', value: vDtTmString, disabled: bOnOffBlockable }).appendTo(divBlDateContainer);
        $('<input />', { type: 'button', id: 'btnPlus', value: '+', style: 'width:30px;height:35px;font-size:25px;border:1px outset #ff0000;text-align:top;vertical-align:middle', disabled: bOnOffBlockable }).appendTo(divBlDateContainer);
        $('<input />', { type: 'button', id: 'btnNow', value: 'NOW', style: 'width:80px;height:35px;font-size:25px;border:1px outset #ff0000;text-align:top;vertical-align:middle' }).appendTo(divBlDateContainer);
        $('<input />', { type: 'button', id: 'btnMinus', value: '-', style: 'width:30px;height:35px;font-size:25px;border:1px outset #ff0000;text-align:top;vertical-align:middle', disabled: bOnOffBlockable }).appendTo(divBlDateContainer);
        $("#divBlDate").append(divBlDateContainer);

        $('#divBlDate input[type=button]').each(function () {
            $(this).on('click', function () { fnNow($(this)); });
        });

        $('#divBlDate input[type=text]').each(function () {
            $(this).on('keyup', function () { fnCheckTimeInput(); });
        });

        vDataOriginal.REM1 = vDataToShow["a_REM1"];
        $('<input />', { type: 'text', id: 'txtRem', style: 'width:250px;height:30px;font-size:25px', maxlength: 250, value: UfisMobile.Core.fnGetValueNonblank(vDataToShow["a_REM1"], "") }).appendTo($("#divRemark"));

        var divClearContainer = $('<div>');
        $('<input />', { type: 'button', id: 'btnClear', value: 'Clear', style: 'width:90px;height:38px;font-size:25px' }).appendTo(divClearContainer);
        $("#divClear").append(divClearContainer);

        var divSaveContainer = $('<div>');
        $('<input />', { type: 'button', id: 'btnSave', value: 'Save', style: 'width:90px;height:38px;font-size:25px' }).appendTo(divSaveContainer);
        $("#divOK").append(divSaveContainer);

        var divCancelContainer = $('<div>');
        $('<input />', { type: 'button', id: 'btnCancel', value: 'Cancel', style: 'width:90px;height:38px;font-size:25px' }).appendTo(divCancelContainer);
        $("#divCancel").append(divCancelContainer);

        $("#btnClear").on('click', function () { fnClear(); });
        $("#btnSave").on('click', function () { fnSave(vDataOriginal); });
        $("#btnCancel").on('click', function () { fnCancel(); });

        //$("#divOK").dxButton({ text: 'Save', clickAction: function () { fnSave(vDataOriginal); } });
        //$("#divCancel").dxButton({ text: 'Cancel', clickAction: function () { fnCancel(); } });

        var dsRG = UfisMobile.Data.PreuseSettings;

        if (vDataToShow["a_ADID"].indexOf("D") >= 0)
            dsRG = UfisMobile.Data.PostuseSettings;

        var divContainer = $('<div id="divPuse" style="color:' + UfisMobile.Theme.ForeColorDark + ';">');
        vDataOriginal.PUSE = UfisMobile.Core.fnGetValueSafe(vDataToShow["a_PUSE"], " ", true);

        //REVIEW
        //This should be done by referring to the actual data set rather than hard-coding
        var vPuseIndx = 0;
        if (vDataOriginal.PUSE === 'K')
            vPuseIndx = 1;
        else if (vDataOriginal.PUSE === 'P')
            vPuseIndx = 2;
        else if (vDataOriginal.PUSE === 'N')
            vPuseIndx = 3;

        divContainer.dxRadioGroup({ dataSource: dsRG, style: 'color:#000000', selectedIndex: vPuseIndx });
        $("#subCnt1").append(divContainer);
    }

    function fnSettingsSetLayout(cnt, vm) {
        var vColorBack = UfisMobile.Theme.BackColorArrD, vColorBack2 = UfisMobile.Theme.BackColorArrL, vColorFore = UfisMobile.Theme.ForeColor;

        var vRet = "<div style=\"display: table;border: solid 0px;width:100%;\">";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Time Zone </div>";
        vRet = vRet + "<div id=\"subCnt1\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Confirm before saving </div>";
        vRet = vRet + "<div id=\"subCnt2\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Inform after saving </div>";
        vRet = vRet + "<div id=\"subCnt3\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "</div>";

        $(cnt).find('*').not('#divPopups').remove();
        $(cnt).append($(vRet));

        var divTimeZone = $('<div id="divTimeZone" style="color:' + vColorFore + ';">');
        var vSelTimeZone = 0;
        if (UfisMobile.Cache.CurrentTimeZone === 'U')
            vSelTimeZone = 1;
        divTimeZone.dxRadioGroup({ dataSource: UfisMobile.Data.TimeZones, displayExpr: 'text', selectedIndex: vSelTimeZone });
        $("#subCnt1").append(divTimeZone);

        var vYNSettings = [{ text: "Yes", status: "Y" }, { text: "No", status: "N" }];

        var divCfmSave = $('<div id="divCfmSave" style="color:' + vColorFore + ';">');
        divCfmSave.dxRadioGroup({ dataSource: vYNSettings, displayExpr: 'text', selectedIndex: 0 });
        $("#subCnt2").append(divCfmSave);

        var divIfmSave = $('<div id="divCfmSave" style="color:' + vColorFore + ';">');
        divIfmSave.dxRadioGroup({ dataSource: vYNSettings, displayExpr: 'text', selectedIndex: 0 });
        $("#subCnt3").append(divIfmSave);
    }

    function fnFilterSetLayout(cnt, vm) {
        var vColorBack = UfisMobile.Theme.BackColorArrD, vColorBack2 = UfisMobile.Theme.BackColorArrL, vColorFore = UfisMobile.Theme.ForeColor;
      
        var vRet = "<div style=\"display: table;border: solid 0px;width:100%;\">";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Flight No </div>";
        vRet = vRet + "<div id=\"subCnt1\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Flight Type </div>";
        vRet = vRet + "<div id=\"subCntFTYP\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Nature Code </div>";
        vRet = vRet + "<div id=\"subCntTTYP\" style=\"display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Flight </div>";
        vRet = vRet + "<div id=\"subCnt2\" style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Terminal </div>";
        vRet = vRet + "<div id=\"subCnt3\" style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Hours Before</div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%;\">";
        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;\">";
        vRet = vRet + "<div id=\"subCnt5\" style=\"text-align:center; display: table-cell;width:20%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "</div>";
        vRet = vRet + "<div id=\"subCnt6\" style=\"display: inline-block; horizontal-align:left; display: table-cell;width:80%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Hours After</div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%;\">";
        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;\">";
        vRet = vRet + "<div id=\"subCnt7\" style=\"text-align:center; display: table-cell;width:20%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "</div>";
        vRet = vRet + "<div id=\"subCnt8\" style=\"horizontal-align:left; display: table-cell;width:80%;height:100%;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\">";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Stand </div>";
        vRet = vRet + "<div id=\"subCnt9\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Pre/Post-use </div>";
        vRet = vRet + "<div id=\"subCntPUSE\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;border: 2px solid " + vColorBack2 + ";width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorFore + ";\">Time Zone </div>";
        vRet = vRet + "<div id=\"subCnt10\" style=\"horizontal-align:left; display: table-cell;background-color:" + vColorBack2 + ";color:" + vColorFore + ";\"></div>";
        vRet = vRet + "</div>";

        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;border: 2px solid " + vColorBack2 + "\">";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div style=\"horizontal-align:left; display: table-cell;width:100%;height:100%;background-color:" + vColorBack + ";\">";
        vRet = vRet + "<div style=\"display: table;border: solid 0px;width:100%; color:" + vColorBack + "\">";
        vRet = vRet + "<div style=\"display: table-row;height:100%;width:100%;\">";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div style=\"display: table-cell;width:40%;height:100%;background-color:" + vColorBack + ";\">.</div>";
        vRet = vRet + "<div id=\"divClear\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "<div id=\"divOK\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "<div id=\"divCancel\" style=\"display: table-cell;width:10%;height:100%;background-color:" + vColorBack + ";\"></div>";
        vRet = vRet + "</div>";
        vRet = vRet + "</div>";

        vRet = vRet + "</div>";

        $(cnt).find('*').not('#divPopups').remove();
        $(cnt).append($(vRet));

        var vRcntFltr = UfisMobile.Cache.RecentFilter;
        var vRcntFlno = vRcntFltr.FLNO;

        var divContainerTxt1 = $('<div id="divFLNO" style="color:red">');
        $('<input />', { type: 'text', id: 'txtFLNO', style: 'width:100px;height:30px;font-size:25px', maxlength: 7, value: vRcntFlno }).appendTo(divContainerTxt1);
        $("#subCnt1").append(divContainerTxt1);

        var vRcntFTYP = UfisMobile.String.fnGetAllAlphas(vRcntFltr.FTYP);
        var divContainerTxtFTYP = $('<div id="divFTYP" style="color:red">');
        $('<input />', { type: 'text', id: 'txtFTYP', style: 'width:80px;height:30px;font-size:25px', maxlength: 5, value: vRcntFTYP }).appendTo(divContainerTxtFTYP);
        $("#subCntFTYP").append(divContainerTxtFTYP);

        var vRcntTTYP = UfisMobile.String.fnGetAllAlphas(vRcntFltr.TTYP);
        var divContainerTxtTTYP = $('<div id="divTTYP" style="color:red">');
        $('<input />', { type: 'text', id: 'txtTTYP', style: 'width:80px;height:30px;font-size:25px', maxlength: 5, value: vRcntTTYP }).appendTo(divContainerTxtTTYP);
        $("#subCntTTYP").append(divContainerTxtTTYP);

        var dsRG = UfisMobile.Data.ADIDSettings;
       
        var divContainerRG = $('<div id="divRG" style="color:' + UfisMobile.Theme.ForeColorDark + ';">');
        var vRcntADID = 2;

        if (vRcntFltr.ADID.indexOf("A") >= 0)
            vRcntADID = 0;
        else if (vRcntFltr.ADID.indexOf("D") >= 0)
            vRcntADID = 1;

        divContainerRG.dxRadioGroup({ dataSource: dsRG, selectedIndex: vRcntADID });
        $("#subCnt2").append(divContainerRG);

        var i;      
        UfisMobile.Application.fnGetTerminalsRecent(vRcntFltr.TRMNLS);
    
        if (UfisMobile.Cache.CurrentTerminals !== null && UfisMobile.Cache.CurrentTerminals.length > 0) { //Checking just for safety
            for (i = 0; i < UfisMobile.Cache.CurrentTerminals.length; i++) {
                var vTer = UfisMobile.Cache.CurrentTerminals[i].text;
                var vTerID = "Terminal" + i;
                var divContainerTmp1 = $('<div>');
                $('<input />', { type: 'checkbox', id: vTerID, value: vTer, checked: UfisMobile.Cache.CurrentTerminals[i].sel }).appendTo(divContainerTmp1);
                $('<label />', { 'for': vTerID, text: vTer }).appendTo(divContainerTmp1);
                $("#subCnt3").append(divContainerTmp1);
            }
        }

        UfisMobile.Application.fnGetPUSEFilterRecent(vRcntFltr.PUSE);

        if (UfisMobile.Cache.CurrentPUSEToFilter !== null && UfisMobile.Cache.CurrentPUSEToFilter.length > 0) { //Checking just for safety
            for (i = 0; i < UfisMobile.Cache.CurrentPUSEToFilter.length; i++) {
                var vPuse = UfisMobile.Cache.CurrentPUSEToFilter[i].text;
                var vPuseID = "ChkPuse" + i;
                var divContainerTmp2 = $('<div>');
                $('<input />', { type: 'checkbox', id: vPuseID, value: vPuse, checked: UfisMobile.Cache.CurrentPUSEToFilter[i].sel }).appendTo(divContainerTmp2);
                $('<label />', { 'for': vPuseID, text: vPuse }).appendTo(divContainerTmp2);
                $("#subCntPUSE").append(divContainerTmp2);
            }
        }

        $('#subCnt3 input[type=checkbox]').each(function () {
            $(this).on('change', function () { UfisMobile.Html.fnTrackChecks($(this), "Terminal", UfisMobile.Cache.CurrentTerminals); });
        });

        $('#subCntPUSE input[type=checkbox]').each(function () {
            $(this).on('change', function () { UfisMobile.Html.fnTrackChecks($(this), "ChkPuse", UfisMobile.Cache.CurrentPUSEToFilter); });
        });

        var fnFilter = function () {
            var vBfSldrFinal = $("#subCnt6").dxSlider("instance");
            var vFrom = vBfSldrFinal.option('value');
            var vAfSldrFinal = $("#subCnt8").dxSlider("instance");
            var vTo = vAfSldrFinal.option('value');

            //var vFlno = $('#subCnt1 input[type=text]')[0].value;
            var vFlno = $("#txtFLNO").val().toUpperCase();

            var vFtyp = UfisMobile.String.fnGetAllAlphas($("#txtFTYP").val().toUpperCase());
            vFtyp = UfisMobile.String.fnGetINCharsDBString(vFtyp);

            var vTtyp = UfisMobile.String.fnGetAllAlphas($("#txtTTYP").val().toUpperCase());
            vTtyp = UfisMobile.String.fnGetINCharsDBString(vTtyp);

            //Get ADID
            var dxrgADID = $("#divRG").dxRadioGroup("instance");
            var vADID = dxrgADID.option('value');

            //Using DevExpress Control (TimeZone)
            var dxrgTZ = $("#divTimeZone").dxRadioGroup("instance");
            var vTZ = dxrgTZ.option('value');

            //Using intrinsic control (Time Zone)
            //var vTZ = $('#divTimeZone').val();            
            
            var strAppend = "", strOrdBy = "";

            //Get Stand
            var dxluStands = $("#divStands").dxLookup("instance");
            var vStand = dxluStands.option('value');
       
            //Get Terminal
            var vTers = UfisMobile.Application.fnGetTerminalsInput(UfisMobile.Cache.CurrentTerminals);

            //Get Puse
            var vPuseFiltered = UfisMobile.Application.fnGetPUSEFilterInput(UfisMobile.Cache.CurrentPUSEToFilter);

            UfisMobile.Cache.CurrentTimeZone = vTZ.status;

            var vSQL = JSON.parse(JSON.stringify(UfisMobile.Constants.FilterSQL));

            if (vADID.text === "Arrival") {
                vSQL.ADID_Both = "";
                vSQL.ADID_D = "";

                vSQL.ADID_A = vSQL.ADID_A.replace("FHOUR_INPUT", vFrom).replace("THOUR_INPUT", vTo);

                if (vStand.length > 0) {
                    vSQL.STAND_Both = "";
                    vSQL.STAND_D = "";
                    vSQL.STAND_A = vSQL.STAND_A.replace("_STAND", vStand);
                }

                if (vTers.length === 0)
                    vSQL.TRMNLS_A = "";
                else
                    vSQL.TRMNLS_A = vSQL.TRMNLS_A.replace(new RegExp("_TRMNLS", "gm"), vTers);

                vSQL.TRMNLS_D = "";
                vSQL.TRMNLS_Both = "";
            }
            else if (vADID.text === "Departure") {
                vSQL.ADID_Both = "";

                vSQL.ADID_D = vSQL.ADID_D.replace("FHOUR_INPUT", vFrom).replace("THOUR_INPUT", vTo);

                vSQL.ADID_A = "";

                if (vStand.length > 0) {
                    vSQL.STAND_Both = "";
                    vSQL.STAND_D = vSQL.STAND_A.replace("_STAND", vStand);
                    vSQL.STAND_A = "";
                }

                vSQL.TRMNLS_A = "";
                if (vTers.length === 0)
                    vSQL.TRMNLS_D = "";
                else
                    vSQL.TRMNLS_D = vSQL.TRMNLS_D.replace(new RegExp("_TRMNLS", "gm"), vTers);

                vSQL.TRMNLS_Both = "";
            }
            else {
                vSQL.ADID_D = "";
                vSQL.ADID_A = "";

                vSQL.ADID_Both = vSQL.ADID_Both.replace(new RegExp("FHOUR_INPUT", "gm"), vFrom).replace(new RegExp("THOUR_INPUT", "gm"), vTo);

                if (vStand.length > 0) {
                    vSQL.STAND_Both = vSQL.STAND_Both.replace(new RegExp("_STAND", "gm"), vStand);
                    vSQL.STAND_D = "";
                    vSQL.STAND_A = "";
                }

                vSQL.TRMNLS_A = "";
                vSQL.TRMNLS_D = "";
                if (vTers.length === 0)
                    vSQL.TRMNLS_Both = "";
                else
                    vSQL.TRMNLS_Both = vSQL.TRMNLS_Both.replace(new RegExp("_TRMNLS", "gm"), vTers);
            }

            if (vStand.length === 0) {
                vSQL.STAND_Both = "";
                vSQL.STAND_D = "";
                vSQL.STAND_A = "";
            }

            if (vFlno !== null && vFlno.trim().length > 0) 
                vSQL.FLNO = vSQL.FLNO.replace("FLNO_INPUT", vFlno);
            else
                vSQL.FLNO = "";

            if (vFtyp.length === 0)
                strAppend = "a.FTYP IN ('O', 'T', 'G')";
            else
                strAppend = "a.FTYP IN (" + vFtyp + ")";

            if (vTtyp.length !== 0)
                strAppend = strAppend + " and a.TTYP IN (" + vTtyp + ")";

            if (vPuseFiltered.length !== 0)
                strAppend = strAppend + " and a.PUSE IN (" + vPuseFiltered + ")";

            strOrdBy = "order by case when (STEV in ('CR') or STE2 in ('CR') or STE3 in ('CR') or STE4 in ('CR')) then 1 else 9 end, DECODE(ADID,'A',TIFA,'D',TIFD,'B',TIFA)";

            try {
                vm.bLoading(true);
                fnRunFilter(UfisMobile.Core.fnGetSQL(vSQL, "AND", strAppend, strOrdBy));
                vm.bLoading(false);
                vm.fnDataRetrieveSuccess();
            } catch (x) {
                vm.bLoading(false);
                vm.fnDataRetrieveError("Filter's got a problem!");
            }
        };

        var fnCancel = function () {
            UfisMobile.app.navigate("Home/Home", { root: true });
        };

        var fnClear= function () {
            var vBfSldrFinal = $("#subCnt6").dxSlider("instance");
            vBfSldrFinal.option('value', 4);//Hard-coded
            var vAfSldrFinal = $("#subCnt8").dxSlider("instance");
            vAfSldrFinal.option('value', 2);//Hard-coded
            $("#txtFLNO").val("");
            $("#txtFTYP").val("");
            var dxluStands = $("#divStands").dxLookup("instance");
            dxluStands.option('value', "");

            //Clear will not clear the Terminal 
            //If this is also to be cleared it needs to handle UfisMobile.Cache.CurrentTerminals in a way rollbackable if the user doesn't use the Filter after Clear
            //document.getElementById("Terminal0").checked = true;
            //fnChange($("#Terminal0"));

            var vADID = $("#divRG").dxRadioGroup("instance");
            vADID.option('selectedIndex', 2);//Hard-coded
        };

        var vRcntFrom = vRcntFltr.FROM * 1;
        var vRcntTo = vRcntFltr.TO * 1;
        $("#subCnt5").text(vRcntFrom);
        $("#subCnt6").dxSlider({ min: 0, max: 4, step: 0.25, height: '20px', value: vRcntFrom, clickAction: function () { fnBeforeSlider(); } });
        $("#subCnt7").text(vRcntTo);
        $("#subCnt8").dxSlider({ min: 0, max: 2, step: 0.25, height: '20px', value: vRcntTo, clickAction: function () { fnAfterSlider(); } });

        var vBfSlider = $("#subCnt6").dxSlider("instance");
        vBfSlider.optionChanged.add(function (n1, v1) {
            $("#subCnt5").text(vBfSlider.option('value'));
        });

        var vAfSlider = $("#subCnt8").dxSlider("instance");
        vAfSlider.optionChanged.add(function (n1, v1) {
            $("#subCnt7").text(vAfSlider.option('value'));
        });

        var vRcntStand = vRcntFltr.STAND;
        var divContainerStand = $('<div id="divStands" style="height:30px;">');
        divContainerStand.dxLookup({ dataSource: UfisMobile.Cache.ParkingStands, displayExpr: 'a_PNAM', valueExpr:'a_PNAM', value: vRcntStand, showClearButton: true });
        $("#subCnt9").append(divContainerStand);

        //Using DevExpress Control (TimeZone)
        var divTimeZone = $('<div id="divTimeZone" style="color:' + UfisMobile.Theme.ForeColorDark + ';">');
        var vSelTimeZone = 0;
        if (UfisMobile.Cache.CurrentTimeZone === 'U')
            vSelTimeZone = 1;
        divTimeZone.dxRadioGroup({ dataSource: UfisMobile.Data.TimeZones, displayExpr: 'text', selectedIndex: vSelTimeZone });
        $("#subCnt10").append(divTimeZone);

        //Using intrinsic control (TimeZone)
        /*var vSelTimeZone = 0;
        if (UfisMobile.Cache.CurrentTimeZone === 'U')
            vSelTimeZone = 1;
        $("#subCnt10").append(UfisMobile.Html.fnGetDropDownList(UfisMobile.Data.TimeZones, "divTimeZone", "text", "status", "width:100px;height:20px;", vSelTimeZone));
        */

        //$("#divOK").dxButton({ text: 'Filter', clickAction: function () { fnFilter(); }});
        //$("#divCancel").dxButton({ text: 'Cancel', clickAction: function () { fnCancel(); }});

        $('<input />', { type: 'button', id: 'btnClear', value: 'Clear', style: 'width:90px;height:38px;font-size:25px' }).appendTo($("#divClear"));
        $('<input />', { type: 'button', id: 'btnFilter', value: 'Filter', style: 'width:90px;height:38px;font-size:25px' }).appendTo($("#divOK"));
        $('<input />', { type: 'button', id: 'btnCancel', value: 'Cancel', style: 'width:90px;height:38px;font-size:25px' }).appendTo($("#divCancel"));

        $("#btnClear").on('click', function () { fnClear(); });
        $("#btnFilter").on('click', function () { fnFilter(); });
        $("#btnCancel").on('click', function () { fnCancel(); });

        $("#txtFTYP").on('keypress', function (evt) { return UfisMobile.Html.fnAcceptOnlyAtoZ(evt); });
        //document.getElementById('txtFTYP').on('keydown', function () { UfisMobile.Html.fnAcceptOnlyAtoZ(document.getElementById('txtFTYP')); });
    }

    function fnRunFilter(strSQL) {
        if (!UfisMobile.Constants.DebugMode) {
            var vParam = UfisMobile.Constants.FilterString.replace("_SELECT", strSQL).replace("_USERNAME", UfisMobile.Cache.CurrentUser);
            var fnSuccess = function (result) {
                UfisMobile.Data.fnGetError(result);
                UfisMobile.Cache.Flights = result;
                UfisMobile.app.navigate("Home/Home", { root: true });
            };
            var fnFail = function (request, status, error) { UfisMobile.Cache.CurrentError = "Fail to run filter"; };
            var fnError = function (error) { UfisMobile.Cache.CurrentError = "Error while running filter."; };
            UfisMobile.Core.fnCallWebSvc(UfisMobile.Constants.ServiceUrl, "POST", "application/json;charset=utf-8", vParam, "json", false, fnSuccess, fnFail, fnError);
        }
        else {
            //For offline development & testing
            //var vParam = UfisMobile.Constants.FilterString.replace("_SELECT", strSQL).replace("_USERNAME", UfisMobile.Cache.CurrentUser);
            UfisMobile.Cache.Flights = UfisMobile.TestData.FlightFilterRecords;
            UfisMobile.app.navigate("Home/Home", { root: true });
        }
    }

    var vTimerID = null;
    function fnSetupFlightListRefresh() {
        if (vTimerID !== null)
            clearTimeout(vTimerID);

        vTimerID = setTimeout(fnRefreshFlightList, UfisMobile.Cache.RefreshRate);
    }

    function fnRefreshFlightList() {
        if (vLoggedIn === false)
            return;

        if (!(UfisMobile.Cache.CurrentLocation === "Home")) {
            fnSetupFlightListRefresh();
            return;
        }

        //fnSetupFlightListRefresh();
        //UfisMobile.app.navigate("Home/Refresh", { root: true });

        //For better refreshing : by not forcing the whold page refresh but just updating the data and front-end UI 
        clearTimeout(vTimerID);
        fnFlightListGetRefreshData();
        fnFlightListGetData();
        vTimerID = setTimeout(fnRefreshFlightList, UfisMobile.Cache.RefreshRate);
        fnFlightListSetLayout(UfisMobile.Cache.CurrentContainers);
        vm.bLoading(false);
    }
  
    var fnShowImp = function (vm, cnts, act, id, adid) {
        try {
            if (vLoggedIn === false) {
                vm.bLoading(false);
                UfisMobile.app.navigate("Login/Logout", { root: true });
            } else {
                UfisMobile.Cache.CurrentLocation = act;
                UfisMobile.Cache.CurrentContainers = cnts;

                //Try the data first
                if (act === "Home") {
                    fnLoadMiscelInfo();
                    //+ Checking the MiscelInfo are OK or not should be done here
                    fnFlightListGetData();
                }
                else if (act === "Refresh") {
                    fnFlightListGetRefreshData();
                    fnSetupFlightListRefresh();
                } else if (act === "Detail") {
                    fnSingleFlightGetData(id);
                }

                //If there is any error trying to get the data from the service
                if (UfisMobile.Cache.CurrentError.length > 0) {//Still not OK
                    vm.bLoading(false);
                    vm.bInfoAlert(true);
                    vm.strInfo("Error in " + act + " page. Error info: " + UfisMobile.Cache.CurrentError);
                }
                else {
                    if (act === "Home") {
                        fnFlightListSetLayout(cnts);
                        vm.bLoading(false);
                    }
                    else if (act === "Refresh") {
                        //fnFlightListSetLayout(cnts);
                        UfisMobile.app.navigate("Home/Home", { root: true });
                        vm.bLoading(false);
                    } else if (act === "Filter") {
                        $(cnts[0]).width('100%');
                        $(cnts[0]).css("float", "none");
                        $(cnts[1]).text("Filter options");
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
                        $(cnts[3]).text("");
                        $(cnts[4]).width('100%');
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';

                        fnFilterSetLayout(cnts[5], vm);
                        vm.bLoading(false);
                    } else if (act === "Settings") {
                        $(cnts[0]).width('100%');
                        $(cnts[0]).css("float", "none");
                        $(cnts[1]).text("Settings");
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
                        $(cnts[3]).text("");
                        $(cnts[4]).width('100%');
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';

                        fnSettingsSetLayout(cnts[5], vm);
                        vm.bLoading(false);
                    } else if (act === "Detail") {
                        $(cnts[0]).width('100%');
                        $(cnts[0]).css("float", "none");
                        $(cnts[1]).text("Flight information");
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[2])).style.marginLeft = '0%';
                        $(cnts[3]).text("");
                        $(cnts[4]).width('100%');
                        document.getElementById(UfisMobile.Html.fnGetIdForJavaScript(cnts[6])).style.marginLeft = '0%';

                        fnSingleFlightSetLayout(cnts[5], adid);
                        vm.bLoading(false);
                    }
                }
            }                   
        } catch (x) {
            vm.strInfo("Error in " + act);
            vm.bLoading(false);
        }        
    };

    var fnLogoutImp = function (vm) {
        vm.strUsr("");
        vm.strPwd("");
        vLoggedIn = false;
        //*Extend
        //Now after logging out the Menu button is still there
        //It's better if the Menu list is hidden then
    };

    UfisMobile.ViewsHandler = {
        fnLogin: fnLoginImp,
        fnShow: fnShowImp,
        fnLogout: fnLogoutImp
    };
})();