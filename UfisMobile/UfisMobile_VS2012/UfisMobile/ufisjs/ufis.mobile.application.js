﻿(function () {
    var fnTranslateFilterImp = function (strFlt) {
        var vFilterSettings = {
            "FROM": "4",
            "TO": "2",
            "FLNO": "",
            "ADID": "",
            "STAND": "",
            "TRMNLS": "",
            "FTYP": "",
            "TTYP": "",
            "PUSE": ""
        };

        if (strFlt !== null && strFlt !== undefined) {
            //forcing it to be a string; if not, sometimes JavaScript engine takes it as an object and indexOf is not working (encountered in the time of development)
            strFlt = strFlt + "";

            //Dropping the oder and any other portions which are not necessary to translate the filter (and can also make the translation wrong)
            var iOrder = strFlt.indexOf("order by");
            if(iOrder>0)
                strFlt = strFlt.substring(0, iOrder);

            var vTmp = "";

            if (strFlt.indexOf("TIFA") >= 0) {
                vFilterSettings.ADID = "A";
            }

            if (strFlt.indexOf("TIFD") >= 0) {
                if (vFilterSettings.ADID === "A")
                    vFilterSettings.ADID = "B";
                else
                    vFilterSettings.ADID = "D";
            }

            vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "JCEDANOW", "JCEDANOW", true);
            vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", "/24", true).trim();
            vFilterSettings.FROM = Math.abs(vTmp * 1);
            vFilterSettings.FROM = vFilterSettings.FROM + "";

            vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "HH24MISS", "HH24MISS", true);
            vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "JCEDANOW", "YYYYMMDD", true).trim();
            vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", "/24", true).trim();
            vFilterSettings.TO = Math.abs(vTmp * 1);
            vFilterSettings.TO = vFilterSettings.TO + "";

            if (strFlt.indexOf("FLNO") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "FLNO", ")", true);
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "%", "%", true);
                vFilterSettings.FLNO = vTmp;
            }

            if (strFlt.indexOf("PSTA") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "PSTA", ")", true);
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "'", "'", true);
                vFilterSettings.STAND = vTmp;
            }
            else if (strFlt.indexOf("PSTD") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "PSTD", ")", true);
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "'", "'", true);
                vFilterSettings.STAND = vTmp;
            }

            if (strFlt.indexOf("TGA1") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "TGA1", ")", false);
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", ")", true);
                vFilterSettings.TRMNLS = vTmp;
            }
            else if (strFlt.indexOf("TGD1") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "TGD1", ")", false);
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", ")", true);
                vFilterSettings.TRMNLS = vTmp;
            }

            if (strFlt.indexOf("FTYP") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "FTYP", ")", false).toUpperCase();
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", ")", true);
                vFilterSettings.FTYP = vTmp;
            }

            if (strFlt.indexOf("TTYP") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "TTYP", ")", false).toUpperCase();
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", ")", true);
                vFilterSettings.TTYP = vTmp;
            }

            if (strFlt.indexOf("PUSE") >= 0) {
                vTmp = UfisMobile.Core.fnGetStringBetween(strFlt, "PUSE", ")", false).toUpperCase();
                vTmp = UfisMobile.Core.fnGetStringBetween(vTmp, "(", ")", true);
                vFilterSettings.PUSE = vTmp;
            }
        }

        return vFilterSettings;
    };

    var fnGetFilterImp = function (objJSON, iVal) {
        //Safety-check
        return objJSON.BODY.ACTS[0].DATA[0][iVal];
    };

    var fnGetScheduleColorImp = function (strADID, strFTYP, strONBL, strOFBL, strTMOA, strLAND, strAIRB) {
        if (strADID === null || strADID === undefined)
            strADID = "";
        if (strFTYP === null || strFTYP === undefined)
            strFTYP = "";
        if (strONBL === null || strONBL === undefined)
            strONBL = "";
        if (strOFBL === null || strOFBL === undefined)
            strOFBL = "";
        if (strTMOA === null || strTMOA === undefined)
            strTMOA = "";
        if (strLAND === null || strLAND === undefined)
            strLAND = "";
        if (strAIRB === null || strAIRB === undefined)
            strAIRB = "";

        strADID = strADID.trim();
        strFTYP = strFTYP.trim();
        strONBL = strONBL.trim();
        strOFBL = strOFBL.trim();
        strTMOA = strTMOA.trim();
        strLAND = strLAND.trim();
        strAIRB = strAIRB.trim();

        var vRet = "";

        //alert("<<<strADID>>>" + strADID + "<<<strFTYP>>>" + strFTYP + "<<<strONBL>>>" + strONBL + "<<<strOFBL>>>" + strOFBL + "<<<strTMOA>>>" + strTMOA + "<<<strLAND>>>" + strLAND + "<<<strAIRB>>>" + strAIRB);

        if (strADID.indexOf("A") >= 0) {
            if (strFTYP === "Z")
                vRet = UfisMobile.Theme.FlightScheduleColor.ArrFtypZ;
            if (strTMOA.length > 0)
                vRet = UfisMobile.Theme.FlightScheduleColor.ArrTMOA;
            if (strLAND.length > 0)
                vRet = UfisMobile.Theme.FlightScheduleColor.ArrLAND;
            if (strONBL.length > 0)
                vRet = UfisMobile.Theme.FlightScheduleColor.ArrONBL;
        }
        else if (strADID.indexOf("D") >= 0) {
            if (strAIRB.length > 0)
                vRet = UfisMobile.Theme.FlightScheduleColor.DepAIRB;
            if (strOFBL.length > 0)
                vRet = UfisMobile.Theme.FlightScheduleColor.DepOFBL;
            if (strFTYP === "Z")
                vRet = UfisMobile.Theme.FlightScheduleColor.DepFtypZ;
        }

        return vRet;
    };

    var fnGetSpecialColorImp = function (strADID, strPUSE, strONBL, strOFBL, strTMOA, strLAND, strAIRB) {
        if (strADID === null || strADID === undefined)
            strADID = "";
        if (strPUSE === null || strPUSE === undefined)
            strPUSE = "";
        if (strONBL === null || strONBL === undefined)
            strONBL = "";
        if (strOFBL === null || strOFBL === undefined)
            strOFBL = "";
        if (strTMOA === null || strTMOA === undefined)
            strTMOA = "";
        if (strLAND === null || strLAND === undefined)
            strLAND = "";
        if (strAIRB === null || strAIRB === undefined)
            strAIRB = "";

        strADID = strADID.trim();
        strPUSE = strPUSE.trim();
        strONBL = strONBL.trim();
        strOFBL = strOFBL.trim();
        strTMOA = strTMOA.trim();
        strLAND = strLAND.trim();
        strAIRB = strAIRB.trim();

        var vRet = "";

        if (strADID.indexOf("A") >= 0) {
            if (strTMOA.length > 0)
                vRet = UfisMobile.Theme.FlightSpecialColor.ArrTMOA;
            if (strLAND.length > 0)
                vRet = UfisMobile.Theme.FlightSpecialColor.ArrLAND;
            if (strONBL.length > 0)
                vRet = UfisMobile.Theme.FlightSpecialColor.ArrONBL;
        }
        else if (strADID.indexOf("D") >= 0) {
            if (strOFBL.length > 0)
                vRet = UfisMobile.Theme.FlightSpecialColor.DepOFBL;
        }

        return vRet;
    };

    //While calling this function, don't pass the uncessary params to get the correct answer
    //i.e to get the PUSE color, don't pass strONBL and strOFBL nad to get the ONBL/OFBL color, don't pass PUSE
    var fnGetCellBgColorImp = function (strRowBgColor, strADID, strONBL, strOFBL, strPUSE) {
        if (strADID === null || strADID === undefined)
            strADID = "";
        if (strRowBgColor === null || strRowBgColor === undefined)
            strRowBgColor = "";
        if (strONBL === null || strONBL === undefined)
            strONBL = "";
        if (strOFBL === null || strOFBL === undefined)
            strOFBL = "";
        if (strPUSE === null || strPUSE === undefined)
            strPUSE = "";


        strADID = strADID.trim();
        strRowBgColor = strRowBgColor.trim();
        strONBL = strONBL.trim();
        strOFBL = strOFBL.trim();
        strPUSE = strPUSE.trim();

        var vRet = "";

        if (strADID.indexOf("A") >= 0) {
            if (strONBL.length > 0 && strRowBgColor === UfisMobile.Theme.FlightSpecialColor.ArrONBL)
                vRet = UfisMobile.Theme.FlightTimingCellBgColor.ArrONBL;

            if (strPUSE.length > 0 && (strPUSE.indexOf("P") >= 0 || strPUSE.indexOf("N") >= 0))
                vRet = UfisMobile.Theme.FlightTimingCellBgColor.ArrPUSEX;
        }
        else if (strADID.indexOf("D") >= 0) {
            if (strOFBL.length > 0 && strRowBgColor === UfisMobile.Theme.FlightSpecialColor.DepOFBL)
                vRet = UfisMobile.Theme.FlightTimingCellBgColor.DepOFBL;

            if (strPUSE.length > 0 && (strPUSE.indexOf("P") >= 0 || strPUSE.indexOf("N") >= 0))
                vRet = UfisMobile.Theme.FlightTimingCellBgColor.DepPUSEX;
        }

        return vRet;
    };


    var fnGetTimingColorImp = function (strFor) {
        //Disable this feature for now
        return "";

        if (strFor === null || strFor === undefined)
            strFor = "";

        var vRet = "";
        $.each(UfisMobile.Theme.TimingColorMatrix, (function (k, v) {
            if (v.value.trim() === strFor.trim()) {
                vRet = v.color;
                return false;
            }
        }));

        return vRet;
    };

    //To extend : A more generalized version
    //Currently not in use
    //Previousl this function is used in breaking down the Towing flights into two flights (one for Dep and one for Arr) just right away
    //***NEED TO VERIFY
    var fnBreakFlgihtsImp = function (dsFlds) {
        var bFound = true;
        var vTmp = {};
        var vPos = -1;
        while (bFound) {
            bFound = false;
            vPos = -1;
            $.each(dsFlds, function (i1, r1) {
                vPos++;
                if (r1.a_ADID === 'B') {
                    bFound = true;
                    vTmp = JSON.parse(JSON.stringify(r1));
                    r1.a_ADID = 'DB';
                    vTmp.a_ADID = 'AB';
                }
            });

            if (bFound)
                UfisMobile.Core.fnInsertIntoArray(dsFlds, vPos, vTmp);
        }

        return dsFlds;
    };

    var fnGetTerminalsInputImp = function (vTrmnl) {
        var vRet = "";
        $.each(vTrmnl, function (i1, r1) {
            if (r1.sel === true && r1.text === "All")
                return false;

            if (r1.sel === true && vRet.length === 0)
                vRet = vRet + "'" + r1.value + "'";
            else if(r1.sel === true)
                vRet = vRet + ",'" + r1.value + "'";
        });

        return vRet;
    };

    var fnGetTerminalsRecentImp = function (strTrmnls) {
        if (strTrmnls === null || strTrmnls === undefined)
            strTrmnls = "";

        if (UfisMobile.Cache.CurrentTerminals === null || UfisMobile.Cache.CurrentTerminals === undefined)
            UfisMobile.Cache.CurrentTerminals = JSON.parse(JSON.stringify(UfisMobile.Data.Terminals));

        if (strTrmnls.length === 0)
            return;

        $.each(UfisMobile.Cache.CurrentTerminals, function (i1, r1) {
            if (r1.text === "All") {
                r1.sel = false;
            }

            if (strTrmnls.indexOf("'" + r1.value + "'") >= 0)
                r1.sel = true;
        });
    };

    var fnGetPUSEFilterInputImp = function (vPuse) {
        var vRet = "";
        $.each(vPuse, function (i1, r1) {
            if (r1.sel === true && r1.text === "All")
                return false;

            if (r1.sel === true && vRet.length === 0)
                vRet = vRet + "'" + r1.status + "'";
            else if (r1.sel === true)
                vRet = vRet + ",'" + r1.status + "'";
        });

        return vRet;
    };

    var fnGetPUSEFilterRecentImp = function (strPuse) {
        if (strPuse === null || strPuse === undefined)
            strPuse = "";

        if (UfisMobile.Cache.CurrentPUSEToFilter === null || UfisMobile.Cache.CurrentPUSEToFilter === undefined)
            UfisMobile.Cache.CurrentPUSEToFilter = JSON.parse(JSON.stringify(UfisMobile.Data.PUSESettingsToFilter));

        if (strPuse.length === 0)
            return;

        $.each(UfisMobile.Cache.CurrentPUSEToFilter, function (i1, r1) {
            if (r1.text === "All") {
                r1.sel = false;
            }

            if (strPuse.indexOf("'" + r1.status + "'") >= 0)
                r1.sel = true;
        });
    };

    var fnArrDepStampOnTowingFlightsImp = function (dsFlds) {
        var bFound = true;
        var vPos = -1;
        while (bFound) {
            bFound = false;
            vPos = -1;
            $.each(dsFlds, function (i1, r1) {
                vPos++;
                if (r1.a_ADID === 'B' && r1.a_FTYP === 'T') {
                    bFound = true;
                    return false;                    
                }
            });

            if (bFound) {
                if (UfisMobile.Core.fnGetValueNonblank(dsFlds[vPos].a_OFBL, "").length === 14) 
                    dsFlds[vPos].a_ADID = "BA";
                else   
                    dsFlds[vPos].a_ADID = "BD";
            }
        }
        return dsFlds;
    };

    var fnRemoveAirborneFlightsImp = function (dsFlds) {        
        var bFound = true;
        var vPos = -1;
        while (bFound) {
            bFound = false;
            vPos = -1;
            $.each(dsFlds, function (i1, r1) {
                vPos++;
                if (r1.a_ADID.indexOf("D") >= 0 && r1.a_AIRB.length > 0 && (!(r1.a_PUSE.indexOf("P")>=0 || r1.a_PUSE.indexOf("N")>=0))) {
                    bFound = true;
                    return false;
                }
            });

            if (bFound) {
                dsFlds.splice(vPos, 1);
            }
        }

        return dsFlds;
    };

    var fnIsCrFlightImp = function (recFl) {
        if (recFl === null || recFl === undefined)
            return false;

        var vCr = "";

        vCr = UfisMobile.Core.fnCheckGetValueNonblank(recFl, "a_STEV", "");
        if (vCr === "CR")
            return true;

        vCr = UfisMobile.Core.fnCheckGetValueNonblank(recFl, "a_STE2", "");
        if (vCr === "CR")
            return true;

        vCr = UfisMobile.Core.fnCheckGetValueNonblank(recFl, "a_STE3", "");
        if (vCr === "CR")
            return true;

        vCr = UfisMobile.Core.fnCheckGetValueNonblank(recFl, "a_STE4", "");
        if (vCr === "CR")
            return true;

        return false;
    };

    var vDtProcessingTimeTrace;
    var vTraceProcessingInfo;
    var fnTraceProcessingImp = function (strPoint) {
        if (vDtProcessingTimeTrace === null || vDtProcessingTimeTrace === undefined) {
            vDtProcessingTimeTrace = new Date();
            vTraceProcessingInfo = strPoint + ":";
            return;
        }

        if (strPoint === null || strPoint === undefined || strPoint.length === 0) {
            vDtProcessingTimeTrace = null;
            alert(vTraceProcessingInfo);
            return;
        }

        var dtTmp = new Date();
        var vMsec = dtTmp.getTime() - vDtProcessingTimeTrace.getTime();
        vTraceProcessingInfo = vTraceProcessingInfo + "\n" + strPoint + " : " + vMsec;
        vDtProcessingTimeTrace = dtTmp;
    };   

    UfisMobile.Application = {
        fnTranslateFilter: fnTranslateFilterImp,
        fnGetFilter: fnGetFilterImp,
        fnGetScheduleColor: fnGetScheduleColorImp,
        fnGetSpecialColor: fnGetSpecialColorImp,
        fnGetCellBgColor: fnGetCellBgColorImp,
        fnGetTimingColor: fnGetTimingColorImp,
        fnBreakFlights: fnBreakFlgihtsImp,
        fnGetTerminalsInput: fnGetTerminalsInputImp,
        fnGetTerminalsRecent: fnGetTerminalsRecentImp,
        fnGetPUSEFilterInput: fnGetPUSEFilterInputImp,
        fnGetPUSEFilterRecent: fnGetPUSEFilterRecentImp,
        fnArrDepStampOnTowingFlights: fnArrDepStampOnTowingFlightsImp,
        fnRemoveAirborneFlights: fnRemoveAirborneFlightsImp,
        fnIsCrFlight: fnIsCrFlightImp,
        fnTraceProcessing: fnTraceProcessingImp
    };
})();