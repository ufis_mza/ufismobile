﻿(function () {
    var fnCallWebSvcImp = function (strUrl, strType, strContentType, vData, strDataType, bAsync, fnSuccess, fnFail, fnError) {
        try {
            $.ajax({
                type: strType,
                url: strUrl,
                data: vData,
                async: bAsync,
                contentType: strContentType,
                dataType: strDataType,
                cache: false,
                success: function (result) { fnSuccess(result); },
                error: function (request, status, error) { fnFail(request, status, error); }
            });
        }
        catch (err) {
            fnError(err);
        }
    };

    var fnTrimArrayContentImp = function (vArr) {
        var i, vTmp;

        if (vArr !== null && vArr.length > 0) {
            for (i = 0; i < vArr.length; i++) {
                vTmp = vArr[i];
                if (vTmp !== null)
                    vArr[i] = vTmp.trim();
            }
        }

        return vArr;
    };

    var fnGetSQLImp = function (vFltrObj, strAppndOprtr, strAppnd, strOrdBy) {
        if (strAppnd === null || strAppnd === undefined)
            strAppnd = "";

        if (strOrdBy === null || strOrdBy === undefined)
            strOrdBy = "";

        if (strAppndOprtr === null || strAppndOprtr === undefined)
            strAppndOprtr = "";
        else
            strAppndOprtr = strAppndOprtr.trim();

        if (strAppndOprtr.length === 0)
            strAppndOprtr = "AND";

        if (vFltrObj === null || vFltrObj === undefined)
            return strAppnd;

        var vFirst = true;
        var vSQLStr = "";

        for (var key in vFltrObj) {
            if (vFltrObj.hasOwnProperty(key)) {//This check is probably EXTRA/NOT-NEEDED
                if (vFltrObj[key] !== null && vFltrObj[key].trim().length > 0) {
                    if (vFirst)
                        vSQLStr = vFltrObj[key].trim();
                    else
                        vSQLStr = vSQLStr + " and " + vFltrObj[key].trim();

                    vFirst = false;
                }
            }
        }

        if (vSQLStr.length > 0 && strAppnd.length > 0) {
            vSQLStr = vSQLStr + " " + strAppndOprtr + " " + strAppnd;

            if (strOrdBy.length > 0)
                vSQLStr = vSQLStr + " " + strOrdBy;
        }

        return vSQLStr;
    };

    var fnGetUpdateSQLImp = function (dOld, dNew, Sprtr) {
        var vRet = { "FIELDS": "", "VALUES": "" };
        $.each(dOld, function (kO, vO) {
            $.each(dNew, function (kN, vN) {
                if (kO === kN && fnAbsEqualImp(vO, vN, false) === false) {
                    if (vRet.FIELDS.length === 0) {
                        vRet.FIELDS = UfisMobile.Data.fnGetUserColumn(kO);
                        vRet.VALUES = vN;
                    }
                    else {
                        vRet.FIELDS = vRet.FIELDS + Sprtr + UfisMobile.Data.fnGetUserColumn(kO);
                        vRet.VALUES = vRet.VALUES + Sprtr + vN;
                    }
                }
            });
        });

        return vRet;
    };

    var fnGetIndexMapImp = function (arrFlds, objFlds) {
        var vRet = {};

        objFlds = fnTrimArrayContentImp(objFlds);
        $.each(arrFlds, function (i, l) {
            if (!(l in vRet))
                vRet[l.replace(".", "_")] = $.inArray(l, objFlds);
        });

        return vRet;
    };

    var fnGetDatasetImp = function (vData, iSt, iEd, strName, iName, iValu, vL2MpIndx, bTrim) {
        var vRows = [];
        var vRet = {};
        var vRetInst = null;
        if (strName === null || strName === undefined || iName < 0)
            strName = "";

        var vTmp;

        $.each(vData, function (i, l) {
            vRetInst = JSON.parse(JSON.stringify(vRet));
            if (strName.length > 0 && vData[i][iName] === strName && i >= iSt) {
                $.each(vL2MpIndx, function (k, v) {
                    if(bTrim === true)
                        vRetInst[k] = fnGetValueNonblankImp(vData[i][iValu][v], "").trim();
                    else
                        vRetInst[k] = fnGetValueNonblankImp(vData[i][iValu][v], "");
                });
                vRows.push(vRetInst);
            }
            else if (strName.length === 0 && i >= iSt && i <= iEd) {
                $.each(vL2MpIndx, function (k, v) {
                    if(bTrim===true)
                        vRetInst[k] = fnGetValueNonblankImp(vData[i][iValu][v], "").trim();
                    else
                        vRetInst[k] = fnGetValueNonblankImp(vData[i][iValu][v], "");
                });
                vRows.push(vRetInst);
            }
        });

        return vRows;
    };

    //To get the value in a safe way to use just by replacing with a safer variable if it is null or undefined
    var fnGetValueSafeImp = function (v, r, bTrim) {
        if (v !== null && v !== undefined) {
            if (bTrim === true)
                return v.trim();

            return v;
        }

        return r;
    };
    
    var fnGetValueNonblankImp = function (v, r, bTrim, strColor) {
        if (strColor === null || strColor === undefined)
            strColor = "";

        if (v === null || v === undefined || v.length === 0 || v.trim().length === 0)
            return r;

        var vRet = v;

        if (bTrim === true)
            vRet = v.trim();

        if (strColor.length > 0) {
            vRet = '<span style="background-color:' + strColor + '">' + vRet + '</span>';
        }

        return vRet;
    };

    var fnCheckGetValueNonblankImp = function (rec, flnm, replc, bTrim, strColor) {
        if (strColor === null || strColor === undefined)
            strColor = "";

        if (replc === null || replc === undefined)
            replc = "";

        if (bTrim === null || bTrim === undefined)
            bTrim = false;

        var vRet = "";
        if (rec === null || rec === undefined)
            vRet = replc;
        else if (rec.hasOwnProperty(flnm)) {
            vRet = rec[flnm];
            if (vRet === null || vRet === undefined || vRet.length === 0 || vRet.trim().length===0)
                vRet = replc;
        }

        if (vRet === null || vRet === undefined)
            vRet = "";

        if (bTrim === true)
            vRet = vRet.trim();

        if (strColor.length > 0) {
            vRet = '<span style="background-color:' + strColor + '">' + vRet + '</span>';
        }

        return vRet;
    }

    var fnAbsEqualImp = function (v1, v2, bTrim) {
        if ((v1 === null || v1 === undefined || v1.trim().length === 0) && (v2 === null || v2 === undefined || v2.trim().length === 0))
            return true;

        if ((!(v1 === null || v1 === undefined || v1.trim().length === 0)) && (v2 === null || v2 === undefined || v2.trim().length === 0))
            return false;

        if ((v1 === null || v1 === undefined || v1.trim().length === 0) && (!(v2 === null || v2 === undefined || v2.trim().length === 0)))
            return false;

        if (bTrim === true)
            return v1.trim() == v2.trim();

        return v1 == v2;
    };

    var fnGetStringBetweenImp = function (str, strSt, strEd, bExclude) {
        var iSt, iEd;
        if (str !== null && str !== undefined && strSt !== null && strSt !== undefined && strEd !== null && strEd !== undefined) {
            if (bExclude)
                iSt = str.indexOf(strSt) + (strSt.length * 1);
            else
                iSt = str.indexOf(strSt);

            if (!bExclude)
                iEd = str.indexOf(strEd, (iSt * 1) + 1) + (strEd.length * 1);
            else
                iEd = str.indexOf(strEd, (iSt * 1) + 1);

            if (iSt >= 0 && iEd > iSt) {
                return str.substring(iSt, iEd);
            }

            return;
        }

        return "";
    };

    var fnJoinTablesImp = function (vTblL, vKeyL1, vColsL, vTblR, vKeyR1, vColsR) {//To extend : with more than one key by using an array 
        if (vTblL === null || vTblL === undefined || vTblR === null || vTblR === undefined)
            return [{}];

        var vRows = [];
        var vRet = {};
        var vRetInst;
        var bAllR = false, bAllL = false;

        if (vColsR === null || vColsR === undefined)
            bAllR = true;

        if (vColsL === null || vColsL === undefined)
            bAllL = true;

        var bFound = false;

        $.each(vTblL, function (i1, r1) {
            vRetInst = JSON.parse(JSON.stringify(vRet));//Deep copy
            for (var key1 in r1) {
                if (!vRetInst.hasOwnProperty(key1) && (bAllL || $.inArray(key1, vColsL) >= 0)) {
                    vRetInst[key1] = r1[key1];
                }
            }

            bFound = false;

            $.each(vTblR, function (i2, r2) {
                if (fnAbsEqualImp(r1[vKeyL1], r2[vKeyR1], true) === true) {
                    for (var key2 in vTblR[0]) {
                        if (!vRetInst.hasOwnProperty(key2) && (bAllR || $.inArray(key2, vColsR) >= 0)) {
                            vRetInst[key2] = r2[key2];
                        }
                    }

                    bFound = true;
                    return false;
                }
            });

            if (!bFound) {
                for (var key3 in vTblR[0]) {
                    if (!vRetInst.hasOwnProperty(key3) && (bAllR || $.inArray(key3, vColsR) >= 0)) {
                        vRetInst[key3] = "";
                    }
                }
            }

            vRows.push(vRetInst);
        });

        return vRows;
    };

    var fnGetDashStringImp = function (vLen, vCol) {
        return "<font color='" + vCol + "'>" + Array(vLen).join(".") + "</font>";
    };

    var fnSetCaretPositionImp = function(elemId, caretPos) {
        var elem = document.getElementById(elemId);

        if (elem !== null) {
            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                if (elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(caretPos, caretPos);
                }
                else
                    elem.focus();
            }
        }
    }

    //To extend 1 : composite key {Now key is only one field}
    //To extend 2 : dsOld.Col1 is to check against dsNew.Col2 {Now fields are taken as having same name 
    //              because the original purpose of this function is to check the updates of dataset which has the exactly same data structure}
    var fnMarkUpdatesImp = function (dsOld, dsNew, key, flds) {
        //Step - 1 : Getting the fields to check/compare
        //Step - 2 : Add a column in dsNew to put in the result info (if not existed yet)
        //Step - 3 : Compare/Check

        //Step - 1 : Getting the fields to check/compare
        //If flds is null, all fileds (expect key) are to be checked
        if (flds === null || flds === undefined) {
            flds = [];
            $.each(dsNew, function (i1, r1) {
                $.each(r1, function (k1) {
                    if(k1!==key && $.inArray(k1, flds)<0)
                        flds.push(k1);
                });

                return false;
            });
        }
        else{
            //If flds have any field which is not in dsNew, remove them
            var ToRemoveNew = [];
            $.each(flds, function (i2, l2) {
                $.each(dsNew, function (i3, r3) {
                    if (!r3.hasOwnProperty(l2)) {
                        ToRemoveNew.push(l2);
                    }

                    return false;
                });
            });

            $.each(ToRemoveNew, function (i4, l4) {
                flds.splice($.inArray(l4, flds), 1);
            });
        }

        //If flds have any field which is not in dsOld, remove them
        var ToRemoveOld = [];
        $.each(flds, function (i5, l5) {
            $.each(dsOld, function (i6, r6) {
                if (!r6.hasOwnProperty(l5)) {
                    ToRemoveOld.push(l5);
                }

                return false;
            });
        });

        $.each(ToRemoveOld, function (i7, l7) {
            flds.splice($.inArray(l7, flds), 1);
        });

        //Step - 2 : Add a column in dsNew to put in the result info (if not existed yet)
        $.each(dsNew, function (i8, r8) {
            if (r8.hasOwnProperty("UPDATED")) {
                r8["UPDATED"].length = 0;
            }
            else {
                r8["UPDATED"] = [];
            }
        });

        //Step - 3 : Compare/Check
        $.each(dsNew, function (i9, r9) {
            var vOldRec = fnGetRecordImp(dsOld, key, r9[key]);
            $.each(flds, function (i10, l10) {
                if (r9.hasOwnProperty(l10) && vOldRec.hasOwnProperty(l10) && (!fnAbsEqualImp(r9[l10], vOldRec[l10], true))) {
                    r9["UPDATED"].push(l10);
                }
            });
        });

        return dsNew;
    };

    var fnGetRecordImp = function (vDs, key, val){
        var vRetRec = {};
        if(vDs === null || vDs === undefined)
            return vRetRec;

        $.each(vDs, function (i1, r1) {
            if(r1.hasOwnProperty(key) && r1[key]===val){
                vRetRec = r1;
                return false;
            }
        });

        return vRetRec;
    };

    var fnInsertIntoArrayImp = function (vArr, index, item) {
        vArr.splice(index, 0, item);
    };

    UfisMobile.Core = {
        fnCallWebSvc: fnCallWebSvcImp,
        fnTrimArrayContent: fnTrimArrayContentImp,
        fnGetSQL: fnGetSQLImp,
        fnGetDataset: fnGetDatasetImp,
        fnGetIndexMap: fnGetIndexMapImp,
        fnGetValueSafe: fnGetValueSafeImp,
        fnGetValueNonblank: fnGetValueNonblankImp,
        fnCheckGetValueNonblank: fnCheckGetValueNonblankImp,
        fnGetUpdateSQL: fnGetUpdateSQLImp,
        fnAbsEqual: fnAbsEqualImp,
        fnGetStringBetween: fnGetStringBetweenImp,
        fnJoinTables: fnJoinTablesImp,
        fnGetDashString: fnGetDashStringImp,
        fnSetCaretPosition: fnSetCaretPositionImp,
        fnMarkUpdates: fnMarkUpdatesImp,
        fnInsertIntoArray: fnInsertIntoArrayImp
    };
})();