﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-10
//Developer     : MZA (Zaw Min Tun)
//Description   : A library of information which will not be changed in the time of running the application

(function () {
    function fnGetAddress() {
        var href = window.location.href.split('/');
        return href[2];
    }    
    
    var vFilterSQL = {
        "ADID_Both": "((a.ADID in ('A','B') AND a.TIFA > TO_CHAR(JCEDANOW-(FHOUR_INPUT/24),'YYYYMMDDHH24MISS') AND a.TIFA < TO_CHAR(JCEDANOW+(THOUR_INPUT/24),'YYYYMMDDHH24MISS')) or (a.ADID in ('B','D') AND a.TIFD > TO_CHAR(JCEDANOW-(FHOUR_INPUT/24),'YYYYMMDDHH24MISS')  AND a.TIFD < TO_CHAR(JCEDANOW+(THOUR_INPUT/24),'YYYYMMDDHH24MISS')))",
        "ADID_A": "(a.ADID in ('A','B') AND a.TIFA > TO_CHAR(JCEDANOW-(FHOUR_INPUT/24),'YYYYMMDDHH24MISS') AND a.TIFA < TO_CHAR(JCEDANOW+(THOUR_INPUT/24),'YYYYMMDDHH24MISS'))",
        "ADID_D": "(a.ADID in ('B','D') AND a.TIFD > TO_CHAR(JCEDANOW-(FHOUR_INPUT/24),'YYYYMMDDHH24MISS')  AND a.TIFD < TO_CHAR(JCEDANOW+(THOUR_INPUT/24),'YYYYMMDDHH24MISS'))",
        "STAND_A": "(a.ADID in ('A','B') AND a.PSTA = '_STAND')",
        "STAND_D": "(a.ADID in ('B', 'D') AND a.PSTD = '_STAND')",
        "STAND_Both": "(a.PSTA = '_STAND' or a.PSTD = '_STAND')",
        "FLNO": "(a.FLNO LIKE '%FLNO_INPUT%')",
        "TRMNLS_A": "(a.TGA1 in (_TRMNLS) or a.TGA2 in (_TRMNLS))",
        "TRMNLS_D": "(a.TGD1 in (_TRMNLS) or a.TGD2 in (_TRMNLS))",
        "TRMNLS_Both": "(a.TGA1 in (_TRMNLS) or a.TGA2 in (_TRMNLS) or a.TGD1 in (_TRMNLS) or a.TGD2 in (_TRMNLS))"
    };

    var vAppName = "Ufis Mobile";
    var vVersion = "1.0.4";//Full information about each version are noted at the end of this file
    var vDebugMode = false;

    var vLoginString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"FIPS\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"GPR\",\"TAB\":\"\",\"FLD\":[\"USID\",\"PASS\",\"APPL\",\"WKST\",\"#PROU\"],\"ODAT\":[],\"DATA\":[\"_USERNAME\",\"_PASSWORD\",\"FIPS\",\"USER\",\"\"],\"ID\":[],\"SEL\":\"\"},{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PAGE_001\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vFlightListString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"FIPS\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\" \",\" \",\"PG_FLTSCH\",\" \",\" \"],\"ID\":[],\"SEL\":\"_SELECT\"}]}}";
    var vFlightQueryString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_FLTSCH\",\"\",\"\"],\"ID\":[],\"SEL\":\"URNO='_URNO'\"}]}}";
    var vFlightUpdateString = "{\"HEAD\":{\"APP\":\"MOBILE\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\":\"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"URNO\",\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"TABN\",\"FLDS\",\"VALU\",\"TYPE\",\"UALL\",\"ULAY\",\"UPAS\",\"UFAL\"],\"ODAT\":[],\"DATA\": [\"20000\",\" \",\" \",\"PG_FLTEDT\",\" \",\"AFTTAB a\",\"_FIELDS\",\"_VALUES\",\"Q\",\"0\",\"0\",\"0\",\"0\"],\"ID\":[],\"SEL\":\"_SELECT\"}]}}";
    var vFlightUpdateStringWithAttnPuse = "{\"HEAD\":{\"APP\":\"MOBILE\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\":\"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"URNO\",\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"TABN\",\"FLDS\",\"VALU\",\"TYPE\",\"UALL\",\"ULAY\",\"UPAS\",\"UFAL\"],\"ODAT\":[],\"DATA\":[\"20000\",\" \",\" \",\"PG_FLTED2\",\" \",\"AFTTAB a\",\"_FIELDS1\",\"_VALUES1\",\"Q\",\"0\",\"0\",\"0\",\"0\"],\"ID\":[],\"SEL\":\"_SELECT\"},{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"URNO\",\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"TABN\",\"FLDS\",\"VALU\",\"TYPE\",\"UALL\",\"ULAY\",\"UPAS\",\"UFAL\"],\"ODAT\":[],\"DATA\":[\"20000\",\" \",\" \",\"PG_CFLIRT\",\" \",\"CFLTAB a\",\"_FIELDS2\",\"_VALUES2\",\"Q\",\"0\",\"0\",\"0\",\"0\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vFilterString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_FILT\",\"\",\"\"],\"ID\":[],\"SEL\":\"_SELECT\"}]}}";
    var vParkingStandsString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_PRKS\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vACTTABString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_ACT\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vALTTABString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_ALT\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vHAITABString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_HAI\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vHAGTABString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"Web Server Application Version Number\",\"IP\":\"Web Server IP\",\"WKS\":\"Web Server Host Name\",\"USR\":\"_USERNAME\",\"HOP\":\"ATH\",\"SCENE\":null,\"REQT\":\"Current App server time in UTC\",\"ORIG\":\"MOBILE\",\"DEST\":\"JCEDA\",\"BCHOST\":\"\",\"BCNUM\":1,\"ID_FLIGHT\":[]},\"BODY\":{\"TRAN\":false,\"TYPE\": \"EXE1FWD2\",\"ACTS\":[{\"CMD\":\"PAGE\",\"TAB\":\"\",\"FLD\":[\"ROLE\",\"GRPN\",\"NAME\",\"ACTN\",\"VALU\"],\"ODAT\":[],\"DATA\":[\"\",\"\",\"PG_HAG\",\"\",\"\"],\"ID\":[],\"SEL\":\"\"}]}}";
    var vUTCDString = "{\"HEAD\":{\"APP\":\"FIPS\",\"VER\":\"5.1.0.0\",\"IP\":\"192.168.56.1\",\"WKS\":\"MZI\",\"USR\":null,\"HOP\":\"WAW\",\"SCENE\":null,\"REQT\":\"20131017104313\"},\"BODY\":{\"TRAN\":false,\"ACTS\":[{\"CMD\":\"GFR\",\"TAB\":\"\",\"FLD\":[],\"DATA\":[],\"SEL\":\"[CONFIG]\"}]}}";

    var vSaveDelay = 1000;

    var vServiceUrl = "http://" + fnGetAddress() + "/KernelCommunication-web/rest/processCommand";

    UfisMobile.Constants = {
        FilterSQL: vFilterSQL,
        ApplicationName: vAppName,
        Version: vVersion,
        AppInfo:  vAppName + " " + vVersion,
        DebugMode: vDebugMode,
        LoginString: vLoginString,
        FlightListString: vFlightListString,
        FlightQueryString: vFlightQueryString,
        FlightUpdateString: vFlightUpdateString,
        FlightUpdateStringWithAttnPuse: vFlightUpdateStringWithAttnPuse,
        FilterString: vFilterString,
        ParkingStandsString: vParkingStandsString,
        ACTTABString: vACTTABString,
        ALTTABString: vALTTABString,
        HAITABString: vHAITABString,
        HAGTABString: vHAGTABString,
        UTCDString: vUTCDString,
        SaveDelay: vSaveDelay,
        ServiceUrl: vServiceUrl
    };
})();

//Version Info
//1.0.1 
//First launch

//1.0.2
//In the time of saving, saving can take some time
//So to call the flight list after saving, a delay (1 sec = 1000 milisec) is put in

//1.0.3
//1 - Remark will also be displayed for Depature Flights and Remark are in BOLD always
//2 - Titles for Depature and Arrival flight list will be center-aligned and bold
//3 - Pre/Post use information words are changed for display : from P to 'PENDING', K to 'OK' and N to 'NO OK'
//4 - User should not be able to enter Onblk prior to landed 
//5 - User should not be able to enter Offblk after Airborne

//1.0.4
//1 - Scrolling is not working in upward directions in Chrome and Opera
//All the .dxScrollView(); in ufis.mobile.viewshandler.js is commented on this account
//2* - This is not a reported issue, but it's found out that the logout behaviour is not correct
//So Logout module is updated accordingly


