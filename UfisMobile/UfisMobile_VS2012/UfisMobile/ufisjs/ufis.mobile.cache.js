﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-10
//Developer     : MZA (Zaw Min Tun)
//Description   : A library of cache information 

(function () {
    var vFlights, vFlightsRecent, vFlightRecent;
    var vRecentFilter;
    var vCurrentUser;
    var vCurrentTimeZone = 'L';
    var vParkingStands;
    var vALTTAB;
    var vACTTAB;
    var vHAGTAB;
    var vHAITAB;
    var vHandlersInfo;
    var vUTCD;
    var vCurrentTerminals;
    var vCurrentError = "";
    var vRefreshRate = 1000 * 60;
    var vCurrentPUSEToFilter;
    var vLastUpdate = null;
    var vCurrentLocation = null;
    var vCurrentContainers = null;
 
    UfisMobile.Cache = {
        Flights: vFlights,
        FlightsRecent: vFlightsRecent,
        FlightRecent: vFlightRecent,
        RecentFilter: vRecentFilter,
        CurrentUser: vCurrentUser,
        CurrentTimeZone: vCurrentTimeZone,
        ParkingStands: vParkingStands,
        ACTTAB: vACTTAB,
        ALTTAB: vALTTAB,
        HAGTAB: vHAGTAB,
        HAITAB: vHAITAB,
        HandlersInfo: vHandlersInfo,
        UTCD: vUTCD,
        CurrentTerminals: vCurrentTerminals,
        CurrentError: vCurrentError,
        RefreshRate: vRefreshRate,
        CurrentPUSEToFilter: vCurrentPUSEToFilter,
        LastUpdate: vLastUpdate,
        CurrentLocation: vCurrentLocation,
        CurrentContainers: vCurrentContainers
    };
})();