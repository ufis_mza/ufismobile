﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-10
//Developer     : MZA (Zaw Min Tun)
//Description   : A library for handling date and time

(function () {
    var fnGetFormattedDateTimeHHMMSlashDayImp = function (v) {
        if (v !== null && v !== undefined) {
            v = v.trim();
            if (v.length === 14)
                return v.substr(8, 2) + ":" + v.substr(10, 2) + " / " + v.substr(6, 2);
        }

        return "";
    };

    var fnGetFormattedDateTimeHHMMImp = function (v) {
        if (v !== null && v !== undefined) {
            v = v.trim();
            if (v.length === 14)
                return v.substr(8, 2) + ":" + v.substr(10, 2);
        }

        return "";
    };

    var fnConvertLocalDateToUTCImp = function (vDt) {
        if (vDt === null || vDt === undefined)
            return null;

        //return new Date(vDt.getUTCFullYear(), vDt.getUTCMonth(), vDt.getUTCDate(), vDt.getUTCHours(), vDt.getUTCMinutes(), vDt.getUTCSeconds());

        var vDtNew = new Date(vDt.getTime());
        var vOffset = (UfisMobile.Cache.UTCD * -1) / 60;
        var vHours = vDt.getHours();
        vDtNew.setHours(vHours + vOffset);
        return vDtNew;
    };

    var fnConvertUTCDateToLocalImp = function (vDt) {
        if (vDt === null || vDt === undefined)
            return null;

        var vDtNew = new Date(vDt.getTime());
        //var vOffset = vDt.getTimezoneOffset() / 60;
        var vOffset = (UfisMobile.Cache.UTCD * -1) / 60;
        var vHours = vDt.getHours();
        vDtNew.setHours(vHours - vOffset);
        return vDtNew;
    };

    var fnY4M2D2H2M2S2ToDateTimeImp = function (vStr) {
        try{
            if (vStr !== null && vStr !== undefined) {
                vStr = vStr.trim();
                if (vStr.length === 14) {
                    var vYr = vStr.substr(0, 4) * 1;
                    var vMn = (vStr.substr(4, 2) * 1) - 1;
                    var vDy = vStr.substr(6, 2) * 1;
                    var vHr = vStr.substr(8, 2) * 1;
                    var vMi = vStr.substr(10, 2) * 1;
                    var vSe = vStr.substr(12, 2) * 1;
                    return new Date(vYr, vMn, vDy, vHr, vMi, vSe);
                }
            }
        } catch (x) {}

        return null;
    };

    var fnDateTimeToY2M2D2H2M2S2Imp = function (vDt) {
        if (vDt === null || vDt === undefined) {
            return "";
        }

        try{
            var vY = vDt.getFullYear();
            var vM = vDt.getMonth() + 1;
            if (vM < 10)
                vM = "0" + vM;
            var vD = vDt.getDate();
            if (vD < 10)
                vD = "0" + vD;
            var vH = vDt.getHours();
            if (vH < 10)
                vH = "0" + vH;
            var vMi = vDt.getMinutes();
            if (vMi < 10)
                vMi = "0" + vMi;
            var vS = vDt.getSeconds();
            if (vS < 10)
                vS = "0" + vS;

            return vY + "" + vM + "" + vD + "" + vH + "" + vMi + "" + vS;
        } catch (x) { }

        return "";
    };

    var fnUfisDateTimeStringToUTCDateTimeImp = function (vUfisDtTmStr) {
        if (vUfisDtTmStr !== null && vUfisDtTmStr !== undefined) {
            return fnY4M2D2H2M2S2ToDateTimeImp(vUfisDtTmStr);
        }

        return null;
    };

    var fnUfisDateTimeStringToLocalDateTimeImp = function (vUfisDtTmStr) {
        if (vUfisDtTmStr !== null && vUfisDtTmStr !== undefined) {
            return fnConvertUTCDateToLocalImp(fnY4M2D2H2M2S2ToDateTimeImp(vUfisDtTmStr));
        }

        return null;
    };

    var fnUTCDateTimeToUfisDateTimeStringImp = function (vDt) {
        if (vDt === null || vDt === undefined) {
            return "";
        }

        return fnDateTimeToY2M2D2H2M2S2Imp(vDt);
    };

    var fnLocalDateTimeToUfisDateTimeStringImp = function (vDt) {
        if (vDt === null || vDt === undefined) {
            return "";
        }

        return fnDateTimeToY2M2D2H2M2S2Imp(fnConvertLocalDateToUTCImp(vDt));
    };

    var fnLocalDateTimeStringToUfisDateTimeStringImp = function (vDtStr) {
        if (vDtStr === null || vDtStr === undefined) {
            return "";
        }

        return fnDateTimeToY2M2D2H2M2S2Imp(fnConvertLocalDateToUTCImp(fnY4M2D2H2M2S2ToDateTimeImp(vDtStr)));
    };

    var fnUfisDateTimeStringToLocalDateTimeStringImp = function (vDtStr) {
        if (vDtStr === null || vDtStr === undefined) {
            return "";
        }

        return fnDateTimeToY2M2D2H2M2S2Imp(fnConvertUTCDateToLocalImp(fnY4M2D2H2M2S2ToDateTimeImp(vDtStr)));
    };

    var fnGetLocalNowStringImp = function (strFrmt) {
        var vDt = new Date();
        vDt = new Date(vDt.getUTCFullYear(), vDt.getUTCMonth(), vDt.getUTCDate(), vDt.getUTCHours(), vDt.getUTCMinutes(), vDt.getUTCSeconds());

        //Following the UTCD of the system setting
        var vOffset = (UfisMobile.Cache.UTCD * -1) / 60;
        var vHours = vDt.getHours();
        vDt.setHours(vHours - vOffset);

        var vH = vDt.getHours();
        if (vH < 10)
            vH = "0" + vH;
        var vMi = vDt.getMinutes();
        if (vMi < 10)
            vMi = "0" + vMi;

        if (strFrmt === "HHMM") {
            return vH + vMi;
        }
        else if (strFrmt === "HH:MM") {
            return vH + ":" + vMi;
        }

        return "";
    };

    var fnGetUTCNowStringImp = function (strFrmt) {
        var vDt = new Date();
        vDt = new Date(vDt.getUTCFullYear(), vDt.getUTCMonth(), vDt.getUTCDate(), vDt.getUTCHours(), vDt.getUTCMinutes(), vDt.getUTCSeconds());

        var vH = vDt.getHours();
        if (vH < 10)
            vH = "0" + vH;
        var vMi = vDt.getMinutes();
        if (vMi < 10)
            vMi = "0" + vMi;

        if (strFrmt === "HHMM") {
            return vH + vMi;
        }
        else if (strFrmt === "HH:MM") {
            return vH + ":" + vMi;
        }

        return "";
    };

    var fnGetTimeStringImp = function (vDt, strFrmt) {
        if (vDt === null || vDt === undefined)
            return "";

        var vH = vDt.getHours();
        if (vH < 10)
            vH = "0" + vH;
        var vMi = vDt.getMinutes();
        if (vMi < 10)
            vMi = "0" + vMi;

        if (strFrmt === "HHMM") {
            return vH + "" + vMi;
        }
        else if (strFrmt === "HH:MM") {
            return vH + ":" + vMi;
        }

        return "";
    };

    var fnIncDateTimeStringImp = function (vDtStr, strFrmt, iAt) {
        if (vDtStr === null || vDtStr === undefined)
            return "";

        if (strFrmt === null || strFrmt === undefined)
            strFrmt = "";

        strFrmt = strFrmt.trim();
        vDtStr = vDtStr.trim();

        if (strFrmt.length === 0) { //No format is given, needs to calcuate in a kind-of-intelligent way
            var vDtStrs, vDtInc = 0, vDtDec = 0, vDtStrReal, vIH, vIM;
            if (vDtStr.indexOf('+') > 0) {
                vDtStrs = vDtStr.split('+');
                vDtInc = vDtStrs[1];
                vDtStrReal = vDtStrs[0];
            }
            else if (vDtStr.indexOf('-') > 0) {
                vDtStrs = vDtStr.split('-');
                vDtDec = vDtStrs[1];
                vDtStrReal = vDtStrs[0];
            }
            else
                vDtStrReal = vDtStr;

            if (vDtStrReal.length === 4 || vDtStrReal.length === 5) { //i.e HHMM or HH:MM
                if (vDtStrReal.length === 5)
                    vDtStrReal = vDtStrReal.match(/\d/g).join("");

                vIH = vDtStrReal.substr(0, 2) * 1;
                vIM = vDtStrReal.substr(2, 2) * 1;

                var vDtTmp = new Date();
                var vDtI = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), vIH, vIM, vDtTmp.getSeconds());
                var vDtO = new Date();
                if (iAt < 2) {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), (vIH * 1) + 1, vIM, vDtTmp.getSeconds());
                }
                else if (iAt >= 2 && iAt < 6) {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), vIH, (vIM * 1) + 1, vDtTmp.getSeconds());
                }
                else {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate() + 1, vIH, vIM, vDtTmp.getSeconds());
                }

                if (vDtI.getDate() < vDtO.getDate()) {
                    if (vDtDec === 0 && vDtInc === 0)
                        vDtInc = (vDtInc * 1) + 1;
                    else if (vDtDec === 0)
                        vDtInc = (vDtInc * 1) + 1;
                    else
                        vDtDec = (vDtDec * 1) - 1;
                }

                if (vDtInc > 0)
                    return fnGetTimeStringImp(vDtO, "HH:MM") + "+" + vDtInc;
                else if (vDtDec > 0)
                    return fnGetTimeStringImp(vDtO, "HH:MM") + "-" + vDtDec;
                else
                    return fnGetTimeStringImp(vDtO, "HH:MM");
            }
            //Other cases : To implemennt
        }
        else if (strFrmt.length === vDtStr.length) {//If format is given, then its length must be equal to that of the data
            //To implement
        }

        return "";
    };

    var fnDecDateTimeStringImp = function (vDtStr, strFrmt, iAt) {
        if (vDtStr === null || vDtStr === undefined)
            return "";

        if (strFrmt === null || strFrmt === undefined)
            strFrmt = "";

        strFrmt = strFrmt.trim();
        vDtStr = vDtStr.trim();

        if (strFrmt.length === 0) { //No format is given, needs to calcuate in a kind-of-intelligent way
            var vDtStrs, vDtInc = 0, vDtDec = 0, vDtStrReal, vIH, vIM;
            if (vDtStr.indexOf('+') > 0) {
                vDtStrs = vDtStr.split('+');
                vDtInc = vDtStrs[1];
                vDtStrReal = vDtStrs[0];
            }
            else if (vDtStr.indexOf('-') > 0) {
                vDtStrs = vDtStr.split('-');
                vDtDec = vDtStrs[1];
                vDtStrReal = vDtStrs[0];
            }
            else
                vDtStrReal = vDtStr;

            if (vDtStrReal.length === 4 || vDtStrReal.length === 5) { //i.e HHMM or HH:MM
                if (vDtStrReal.length === 5)
                    vDtStrReal = vDtStrReal.match(/\d/g).join("");

                vIH = vDtStrReal.substr(0, 2) * 1;
                vIM = vDtStrReal.substr(2, 2) * 1;

                var vDtTmp = new Date();
                var vDtI = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), vIH, vIM, vDtTmp.getSeconds());
                var vDtO = new Date();
                if (iAt < 2) {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), (vIH * 1) - 1, vIM, vDtTmp.getSeconds());
                }
                else if (iAt >= 2 && iAt < 6) {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate(), vIH, (vIM * 1) - 1, vDtTmp.getSeconds());
                }
                else {
                    vDtO = new Date(vDtTmp.getFullYear(), vDtTmp.getMonth(), vDtTmp.getDate() - 1, vIH, vIM, vDtTmp.getSeconds());
                }

                if (vDtI.getDate() > vDtO.getDate()) {
                    if (vDtDec === 0 && vDtInc === 0)
                        vDtDec = (vDtDec * 1) - 1;
                    else if (vDtDec === 0)
                        vDtInc = (vDtInc * 1) - 1;
                    else
                        vDtDec = (vDtDec * 1) + 1;
                }

                if (vDtInc > 0)
                    return fnGetTimeStringImp(vDtO, "HH:MM") + "+" + vDtInc;
                else if (vDtDec > 0)
                    return fnGetTimeStringImp(vDtO, "HH:MM") + "-" + vDtDec;
                else
                    return fnGetTimeStringImp(vDtO, "HH:MM");
            }
            //Other cases : To implemennt
        }
        else if (strFrmt.length === vDtStr.length) {//If format is given, then its length must be equal to that of the data
            //To implement
        }

        return "";
    };

    //(vDtTmStr => 20141112130100, vStr => 14:10+2) => 20141114141000             
    var fnConjugateDateTimeStringImp = function (vDtTmStr, vStr) {
        if (vDtTmStr === null || vDtTmStr === undefined)
            return "";

        if (vStr === null || vStr === undefined)
            return "";

        vStr = vStr.trim();
        if (vStr.length === 0)
            return "";

        vDtTmStr = vDtTmStr.trim();

        if (vDtTmStr.length !== 14)
            return "";

        var vStrs, vDtInc = 0, vDtDec = 0, vDtStrReal;
        if (vStr.indexOf('+') > 0) {
            vStrs = vStr.split('+');
            vDtInc = vStrs[1];
            vDtStrReal = vStrs[0];
        }
        else if (vStr.indexOf('-') > 0) {
            vStrs = vStr.split('-');
            vDtDec = vStrs[1];
            vDtStrReal = vStrs[0];
        }
        else
            vDtStrReal = vStr;

        //vDtStrReal = vDtStrReal.replace(/^\D+/g, ''); //NOT WORKING
        vDtStrReal = vDtStrReal.match(/\d/g).join("");

        var vH = vDtStrReal.substr(0, 2) * 1;
        var vMi = vDtStrReal.substr(2, 2) * 1;

        var vDtI = fnY4M2D2H2M2S2ToDateTimeImp(vDtTmStr);
        var vDayOfM = (vDtI.getDate() * 1) + (vDtInc * 1) - (vDtDec * 1);
        var vDtO = new Date(vDtI.getFullYear(), vDtI.getMonth(), vDayOfM, vH, vMi, vDtI.getSeconds());
        return fnDateTimeToY2M2D2H2M2S2Imp(vDtO);
    };

    var fnCheckTimeImp = function (strTmString, strFrmt, iAt) {
        if (strTmString === null || strTmString === undefined)
            return "";

        if (strFrmt === null || strFrmt === undefined)
            strFrmt = "";

        strTmString = strTmString.trim();

        var vStrs, vDtInc = 0, vDtDec = 0, vDtStrReal, bPlus = false, bMinus = false;
        var vH, vMi;
        if (strTmString.indexOf('+') > 0) {
            vStrs = strTmString.split('+');
            vDtInc = vStrs[1];
            vDtStrReal = vStrs[0];
            bPlus = true;
        }
        else if (strTmString.indexOf('-') > 0) {
            vStrs = strTmString.split('-');
            vDtDec = vStrs[1];
            vDtStrReal = vStrs[0];
            bMinus = true;
        }
        else
            vDtStrReal = strTmString;

        var vMiStr = "";
        if (vDtStrReal.indexOf(':') >= 0) {
            vStrs = strTmString.split(':');
            vH = vStrs[0];
            vMi = vStrs[1];
            vMiStr = vStrs[1] + "";
        } else {
            if (vDtStrReal.length >= 2)
                vH = vDtStrReal.substr(0, 2);
            else
                vH = vDtStrReal.substr(0, vDtStrReal.length);

            if (vDtStrReal.length > 2) {
                vMi = vDtStrReal.substr(2, vDtStrReal.length - 2);
                vMiStr = vDtStrReal.substr(2, vDtStrReal.length - 2) + "";
            }
            else {
                vMi = 0;
                vMiStr = "";
            }

            vH = vH * 1;
            vMi = vMi * 1;
        }

        if (vH > 23 || vMi > 59 || (vMiStr.length === 1 && vMi > 5)) {
            if (iAt > 0) {
                if (vDtStrReal.length > iAt) {
                    vDtStrReal = vDtStrReal.substr(0, iAt - 1) + vDtStrReal.substr(iAt, vDtStrReal.length - iAt);
                }
                else
                    vDtStrReal = vDtStrReal.substr(0, iAt - 1);
            } else {
                vDtStrReal = "23:59";
            }
        }

        if (vDtDec > 0)
            vDtStrReal = vDtStrReal + "-" + vDtDec;
        else if (vDtInc > 0)
            vDtStrReal = vDtStrReal + "+" + vDtInc;
        else if (bPlus)
            vDtStrReal = vDtStrReal + "+";
        else if (bMinus)
            vDtStrReal = vDtStrReal + "-";

        return vDtStrReal;
    };
  
    UfisMobile.DateTime = {
        fnGetFormattedDateTimeHHMMSlashDay: fnGetFormattedDateTimeHHMMSlashDayImp,
        fnGetFormattedDateTimeHHMM: fnGetFormattedDateTimeHHMMImp,
        fnConvertLocalDateToUTC: fnConvertLocalDateToUTCImp,
        fnConvertUTCDateToLocal: fnConvertUTCDateToLocalImp,
        fnY4M2D2H2M2S2ToDateTime: fnY4M2D2H2M2S2ToDateTimeImp,
        fnDateTimeToY2M2D2H2M2S2: fnDateTimeToY2M2D2H2M2S2Imp,
        fnUfisDateTimeStringToUTCDateTime: fnUfisDateTimeStringToUTCDateTimeImp,
        fnUfisDateTimeStringToLocalDateTime: fnUfisDateTimeStringToLocalDateTimeImp,
        fnUTCDateTimeToUfisDateTimeString: fnUTCDateTimeToUfisDateTimeStringImp,
        fnLocalDateTimeToUfisDateTimeString: fnLocalDateTimeToUfisDateTimeStringImp,
        fnLocalDateTimeStringToUfisDateTimeString: fnLocalDateTimeStringToUfisDateTimeStringImp,
        fnUfisDateTimeStringToLocalDateTimeString: fnUfisDateTimeStringToLocalDateTimeStringImp,
        fnGetLocalNowString: fnGetLocalNowStringImp,
        fnGetUTCNowString: fnGetUTCNowStringImp,
        fnGetTimeString: fnGetTimeStringImp,
        fnIncDateTimeString: fnIncDateTimeStringImp,
        fnDecDateTimeString: fnDecDateTimeStringImp,
        fnConjugateDateTimeString: fnConjugateDateTimeStringImp,
        fnCheckTime: fnCheckTimeImp
    };
})();