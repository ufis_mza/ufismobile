﻿//Company       : Ufis Airport Solutions
//Date          : 2014-02-20
//Developer     : MZA (Zaw Min Tun)
//Description   : A library to handle string-related facilities

(function () {
    var fnGetAllAlphasImp = function (str, strIgnores, bCaseSensitive) {
        if (str === null || str === undefined)
            str = "";

        if (strIgnores === null || strIgnores === undefined)
            strIgnores = "";

        if (bCaseSensitive === null || bCaseSensitive === undefined)
            bCaseSensitive = false;

        var strAlphas = "";
        var vstrIgnoresU = strIgnores.toUpperCase();
        var vstrIgnoresL = strIgnores.toLowerCase();
        
        if (bCaseSensitive) {
            for (var i = 0; i < str.length; i++) {
                if (str[i] >= 'A' && str[i] <= 'z' && (!(strIgnores.indexOf(str[i]) >= 0)))
                    strAlphas += str[i];
            }
        }
        else {
            for (var i = 0; i < str.length; i++) {
                if (str[i] >= 'A' && str[i] <= 'z' && (!((vstrIgnoresU.indexOf(str[i]) >= 0) || (vstrIgnoresL.indexOf(str[i]) >= 0))))
                    strAlphas += str[i];
            }
        }

        return strAlphas;
    };

    //Input : ABC
    //Output : 'A','B','C'
    //If strAddional is AE then output will be'A','B','C', 'E'
    var fnGetINCharsDBStringImp = function (str, strAdditional) {
        if (str === null || str === undefined)
            str = "";

        if (strAdditional === null || strAdditional === undefined)
            strAdditional = "";

        var i, strRet = "";

        for (i = 0; i < strAdditional.length; i++) {
            if (!(str.indexOf(strAdditional[i]) >= 0)) {
                str += strAdditional[i];
            }
        }

        for (i = 0; i < str.length; i++) {
            if(i===0)
                strRet = "'" + str[i] + "'";
            else
                strRet = strRet + ",'" + str[i] + "'";
        }

        return strRet;
    };

    UfisMobile.String = {
        fnGetAllAlphas: fnGetAllAlphasImp,
        fnGetINCharsDBString: fnGetINCharsDBStringImp
    };
})();